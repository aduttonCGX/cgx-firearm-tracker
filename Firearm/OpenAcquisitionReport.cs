﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using Utilities;
using System.IO;


namespace Firearm
{
    public class OpenAcquisitionReport : FirearmReport
    {
        private readonly bool hasDateRange;
        public OpenAcquisitionReport(DateTime startDate, DateTime endDate, User user, Store store, OrderByColumn column)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.user = user;
            this.store = store;
            this.column = column;
            hasDateRange = true;
        }
        public OpenAcquisitionReport(User user, Store store, OrderByColumn column)
        {
            startDate = null;
            endDate = null;
            this.user = user;
            this.store = store;
            this.column = column;
            hasDateRange = false;
        }
        public override void QueryData()
        {
            records = new List<Record>();
            try
            {
                SqlDataReader reader = null;
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_GetOpenAcquisitionReportData", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@StoreID", store.StoreID));
                    if (startDate == null)
                        cmd.Parameters.Add(new SqlParameter("@StartDate", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@StartDate", startDate.Value));
                    if (endDate == null)
                        cmd.Parameters.Add(new SqlParameter("@EndDate", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@EndDate", endDate.Value));
                    switch (column)
                    {
                        case OrderByColumn.AcquisitionID:
                            cmd.Parameters.Add(new SqlParameter("@SortColumn", "AcquisitionID"));
                            break;
                        case OrderByColumn.BinderNumber:
                            cmd.Parameters.Add(new SqlParameter("@SortColumn", "BinderNumber"));
                            break;
                    }
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Record record = new Record
                        {
                            AcquisitionID = int.Parse(reader["AcquisitionID"].ToString()),
                            AcquisitionDate = DateTime.Parse(reader["AcquisitionDate"].ToString()),
                            DateAcquisitionCreated = DateTime.Parse(reader["DateAcquisitionCreated"].ToString()),
                            DateAcquisitionLastUpdated = DateTime.Parse(reader["DateAcquisitionUpdated"].ToString()),
                            SerialNumber = reader["SerialNumber"].ToString(),
                            Manufacturer = reader["ManufacturerDescription"].ToString(),
                            Model = reader["ModelDescription"].ToString(),
                            Type = reader["TypeDescription"].ToString(),
                            CaliberOrGauge = reader["CaliberGaugeDescription"].ToString(),
                            StoreAbbreviation = reader["StoreAbbreviation"].ToString(),
                            StoreDescription = reader["StoreDescription"].ToString(),
                            SupplierName = reader["SupplierDescription"].ToString()
                        };

                        if (int.TryParse(reader["BinderNumber"].ToString(), out int tempBinderNumber))
                        {
                            record.BinderNumber = tempBinderNumber;
                        }
                        else
                        {
                            record.BinderNumber = 0;
                        }

                        if (int.TryParse(reader["LineItemNumber"].ToString(), out int tempLineItemNumber))
                        {
                            record.LineItemNumber = tempLineItemNumber;
                        }
                        else
                        {
                            record.LineItemNumber = 0;
                        }
                        records.Add(record);
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }
        }
        public override void BuildReport()
        {
            {
                string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Firearm Reports\\";
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string fileName = $"OpenAcquisitionReport_{DateTime.Today.ToString("yyyyMMdd")}_000.pdf";

                // Make sure you're writing to a new file
                Directory.CreateDirectory(directory);
                int fileNumber = 0;
                while (File.Exists(Path.Combine(directory, fileName)))
                {
                    fileNumber++;
                    fileName = $"OpenAcquisitionReport_{DateTime.Today.ToString("yyyyMMdd")}_{fileNumber.ToString("D" + 3)}.pdf";
                }

                try
                {
                    // Start Excel
                    Application excel = new Application
                    {
                        Visible = false,
                        DisplayAlerts = false
                    };
                    Workbook workbook = excel.Workbooks.Add(Type.Missing);
                    Worksheet worksheet = workbook.ActiveSheet;
                    worksheet.Name = $"OpenAcquisitionReport";
                    Range headerRange, formatRange, itemRowRange;

                    // Margins 
                    worksheet.PageSetup.Orientation = XlPageOrientation.xlPortrait;
                    worksheet.PageSetup.Zoom = false;
                    worksheet.PageSetup.FitToPagesTall = false;
                    worksheet.PageSetup.FitToPagesWide = 1;
                    worksheet.PageSetup.TopMargin = excel.InchesToPoints(0.75);
                    worksheet.PageSetup.BottomMargin = excel.InchesToPoints(0.75);
                    worksheet.PageSetup.LeftMargin = excel.InchesToPoints(0.25);
                    worksheet.PageSetup.RightMargin = excel.InchesToPoints(0.25);
                    worksheet.PageSetup.CenterHorizontally = true;

                    // Column widths
                    worksheet.Columns["A:A"].ColumnWidth = 13;
                    worksheet.Columns["B:B"].ColumnWidth = 8;
                    worksheet.Columns["C:C"].ColumnWidth = 16;
                    worksheet.Columns["D:D"].ColumnWidth = 34;
                    worksheet.Columns["E:E"].ColumnWidth = 25;

                    // Report Header
                    headerRange = (Range)worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 5]];
                    headerRange.Merge();
                    headerRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    headerRange.VerticalAlignment = XlVAlign.xlVAlignCenter;
                    headerRange.Font.Size = 20;
                    headerRange.Font.Bold = true;
                    headerRange.Font.Color = ColorTranslator.ToOle(Color.White);
                    headerRange.Interior.Color = ColorTranslator.ToOle(Color.Gray);
                    headerRange.RowHeight = 36;
                    worksheet.Cells[1, 1] = "Open Acquisition Report";

                    // Report parameters
                    formatRange = (Range)worksheet.Range[worksheet.Cells[2, 1], worksheet.Cells[3, 1]];
                    formatRange.HorizontalAlignment = XlHAlign.xlHAlignRight;
                    formatRange.Font.Bold = true;
                    worksheet.Cells[2, 1] = "Store:";
                    worksheet.Cells[3, 1] = "Dates:";

                    formatRange = (Range)worksheet.Range[worksheet.Cells[2, 2], worksheet.Cells[3, 2]];
                    formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                    worksheet.Cells[2, 2] = $"{store.StoreID} {store.StoreDescription}";
                    worksheet.Cells[3, 2] = hasDateRange ? $"{startDate.Value.ToShortDateString()} - {endDate.Value.ToShortDateString()}" : "All";
                    int row = 5;

                    // Data Headers
                    if (records.Count > 0)
                    {
                        // Column headers
                        formatRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 5]];
                        formatRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                        formatRange.VerticalAlignment = XlVAlign.xlVAlignBottom;
                        formatRange.Font.Underline = true;
                        formatRange.Font.Bold = true;
                        worksheet.Cells[row, 1] = "Acquisition ID";
                        worksheet.Cells[row, 2] = "Binder";
                        worksheet.Cells[row, 3] = "Acquisition Date";
                        worksheet.Cells[row, 4] = "Item";
                        worksheet.Cells[row, 5] = "Serial Number";
                        row++;

                        // Data
                        for (int item = 0; item < records.Count; item++)
                        {   
                            itemRowRange = (Range)worksheet.Range[worksheet.Cells[row, 1], worksheet.Cells[row, 5]];
                            itemRowRange.Font.Size = 10;
                            itemRowRange.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                            itemRowRange.VerticalAlignment = XlVAlign.xlVAlignTop;
                            itemRowRange.NumberFormat = "@";
                            itemRowRange.WrapText = true;
                            if (item % 2 == 1)
                            {
                                itemRowRange.Interior.Color = ColorTranslator.ToOle(Color.LightGray);
                            }
                            worksheet.Cells[row, 1] = records[item].AcquisitionID;
                            worksheet.Cells[row, 2] = $"{records[item].BinderNumber}-{records[item].LineItemNumber}";
                            worksheet.Cells[row, 3].NumberFormat = "MM/DD/YYYY";
                            worksheet.Cells[row, 3] = records[item].AcquisitionDate;
                            worksheet.Cells[row, 4] = $"{records[item].Manufacturer} {records[item].Model} {Environment.NewLine}{records[item].Type} {records[item].CaliberOrGauge}";
                            worksheet.Cells[row, 5] = records[item].SerialNumber;
                            row++;
                        }
                    }

                    // Total number of records
                    worksheet.Cells[row + 1, 5] = $"Total Records: {records.Count}";

                    // Page Numbering
                    worksheet.PageSetup.LeftFooter = $"Open Aquisition Report";
                    worksheet.PageSetup.CenterFooter = "&P/&N";
                    worksheet.PageSetup.RightFooter = $"{DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")}";

                    // Save and close
                    workbook.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, Path.Combine(directory, fileName));
                    workbook.Close();
                    excel.Quit();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                // Open the folder
                bool explorerOpen = false;
                Process[] processes = Process.GetProcessesByName("explorer");
                foreach (Process process in processes)
                {
                    if (process.MainWindowTitle == "Firearm Reports")
                    {
                        explorerOpen = true;
                        break;
                    }
                }
                if (!explorerOpen)
                    Process.Start("explorer.exe", directory);
            }
        }
    }
}
