﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Model
    {
        public int ModelID { get; }
        public int ManufacturerID { get; }
        public string ModelDescription { get; }

        public Model(int id, int manufacturer, string description)
        {
            ModelID = id;
            ManufacturerID = manufacturer;
            ModelDescription = description;
        }
    }
}
