﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Store
    {
        public string StoreID { get; set; }
        public string StoreAbbreviation { get; set; }
        public string StoreDescription { get; set; }
        public string DisplayDescription { get => $"{StoreID} {StoreDescription}"; }

        public Store(string storeID, string storeAbbreviation, string storeDescription)
        {
            StoreID = storeID;
            StoreAbbreviation = storeAbbreviation;
            StoreDescription = storeDescription;
        }
    }
}
