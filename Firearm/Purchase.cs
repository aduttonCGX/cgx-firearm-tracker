﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Purchase
    {
        private int acquisitionID;
        private DateTime dispositionDate;
        private string form4473SerialNumber;
        private string weaponDetails;
        private string store;

        public int AcquisitionID { get => acquisitionID; set => acquisitionID = value; }
        public DateTime DispositionDate { get => dispositionDate; set => dispositionDate = value; }
        public string Form4473SerialNumber { get => form4473SerialNumber; set => form4473SerialNumber = value; }
        public string WeaponDetails { get => weaponDetails; set => weaponDetails = value; }
        public string Store { get => store; set => store = value; }

        public override string ToString()
        {
            return $"{Environment.NewLine}AcquisitionID: {acquisitionID} {Environment.NewLine}Disposition Date: {dispositionDate.ToShortDateString()} {Environment.NewLine}Form 4473 SN: {form4473SerialNumber} {Environment.NewLine}Item: {weaponDetails}{Environment.NewLine}Selling Store: {store}";
        }
    }
}
