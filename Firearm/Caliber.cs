﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Caliber
    {
        public int CaliberID { get; }
        public string CaliberDescription { get; }

        public Caliber(int id, string description)
        {
            CaliberID = id;
            CaliberDescription = description;
        }
    }
}
