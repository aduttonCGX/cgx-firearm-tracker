﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class RecordRandomizer
    {
        private Random random;

        public Record CreateRandomRecord()
        {

            Record returnRecord = new Record();

            returnRecord.AcquisitionID = random.Next(1000);
            switch (random.Next(4))
            {
                case 1:
                    returnRecord.Manufacturer = "Smith & Wesson";
                    returnRecord.Model = "SW1911E";
                    returnRecord.SerialNumber = $"UFC{random.Next(1000, 9999).ToString()}";
                    returnRecord.Type = "Pistol";
                    returnRecord.CaliberOrGauge = ".45 ACP";
                    returnRecord.SupplierName = "TGD";
                    returnRecord.SupplierLicenseNumber = "FFL# 1-56-125-01-1A-03909";
                    break;
                case 2:
                    returnRecord.Manufacturer = "Smith & Wesson";
                    returnRecord.Model = "M&P Shield 2.0";
                    returnRecord.SerialNumber = $"HYX{random.Next(1000, 9999).ToString()}";
                    returnRecord.Type = "Pistol";
                    returnRecord.CaliberOrGauge = "9mm";
                    returnRecord.SupplierName = "TGD";
                    returnRecord.SupplierLicenseNumber = "FFL# 1-56-125-01-1A-03909";
                    break;
                case 3:
                    returnRecord.Manufacturer = "Kimber";
                    returnRecord.Model = "Micro 9 Raptor";
                    returnRecord.SerialNumber = $"PB0{random.Next(100000, 999999).ToString()}";
                    returnRecord.Type = "Pistol";
                    returnRecord.CaliberOrGauge = "9mm";
                    returnRecord.SupplierName = "KIMBER";
                    returnRecord.SupplierLicenseNumber = "FFL# 6-13-119-07-1A-00458";
                    break;
                default:
                    returnRecord.Manufacturer = "Glock";
                    returnRecord.Model = "G43X";
                    returnRecord.SerialNumber = $"BLCW{random.Next(100, 999).ToString()}";
                    returnRecord.Type = "Pistol";
                    returnRecord.CaliberOrGauge = "9mm";
                    returnRecord.SupplierName = "Kiester's Police Supply";
                    returnRecord.SupplierLicenseNumber = "4-35-019-11-0o-01674";
                    break;
            }

            returnRecord.AcquisitionDate = DateTime.Now.AddDays(random.Next(21) * -1);

            switch (random.Next(6))
            {
                case 1:
                    returnRecord.StoreID = 601;
                    break;
                case 2:
                    returnRecord.StoreID = 201;
                    break;
                case 3:
                    returnRecord.StoreID = 459;
                    break;
                case 4:
                    returnRecord.StoreID = 901;
                    break;
                case 5:
                    returnRecord.StoreID = 511;
                    break;
                default:
                    returnRecord.StoreID = 501;
                    break;
            }
            
            returnRecord.DateAcquisitionCreated = new DateTime(DateTime.Today.Year, DateTime.Today.Month, random.Next(1, DateTime.Today.Day));
            returnRecord.UserAcquisitionCreated = "aduttonadm";
            returnRecord.DateAcquisitionLastUpdated = new DateTime(DateTime.Today.Year, DateTime.Today.Month, random.Next(1, DateTime.Today.Day));
            returnRecord.UserAcquisitionLastUpdated = "aduttonadm";

            // 16% chance of being deleted
            switch (random.Next(10))
            {
                case 1:
                    returnRecord.DateDeleted = DateTime.Today;
                    returnRecord.UserDeleted = "aduttonadm";
                    break;
                default:
                    // 25% chance of being open
                    switch (random.Next(4))
                    {
                        case 1:
                            returnRecord.DispositionID = null;
                            returnRecord.PurchaserFirstName = null;
                            returnRecord.PurchaserLastName = null;
                            returnRecord.PurchaserAddress1 = null;
                            returnRecord.PurchaserAddress2 = null;
                            returnRecord.PurchaserCity = null;
                            returnRecord.PurchaserState = null;
                            returnRecord.PurchaserZip = null;
                            returnRecord.PurchaserCountry = null;
                            returnRecord.PrimaryIdentificationNumber = null;
                            returnRecord.Form4473SerialNumber = null;
                            returnRecord.DateDispositionCreated = null;
                            returnRecord.UserDispositionCreated = null;
                            returnRecord.DateDispositionLastUpdated = null;
                            returnRecord.UserDispositionLastUpdated = null;
                            break;
                        default:
                            returnRecord.DispositionID = random.Next(1000);
                            returnRecord.PurchaserFirstName = "John";
                            returnRecord.PurchaserMiddleName = "Steven";
                            returnRecord.PurchaserLastName = "Doe";
                            switch (random.Next(3))
                            {
                                case 1:
                                    returnRecord.PurchaserAddress1 = $"{random.Next(100, 9999).ToString()} Fake Street";
                                    returnRecord.PurchaserAddress2 = $"Apt {random.Next(9).ToString()}";
                                    returnRecord.PurchaserCity = "Chesapeake";
                                    returnRecord.PurchaserState = "VA";
                                    returnRecord.PurchaserZip = "23320";
                                    returnRecord.PurchaserCountry = "US";
                                    break;
                                case 2:
                                    returnRecord.PrimaryIdentificationNumber = $"2018-0{random.Next(100, 999).ToString()}";
                                    break;
                                default:
                                    returnRecord.Form4473SerialNumber = $"2019-0{random.Next(100, 999).ToString()}";
                                    break;
                            }
                            returnRecord.DispositionDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, random.Next(1, DateTime.Today.Day));
                            returnRecord.DateDispositionCreated = new DateTime(DateTime.Today.Year, DateTime.Today.Month, random.Next(1, DateTime.Today.Day));
                            returnRecord.UserDispositionCreated = "aduttonadm";
                            returnRecord.DateDispositionLastUpdated = new DateTime(DateTime.Today.Year, DateTime.Today.Month, random.Next(1, DateTime.Today.Day));
                            returnRecord.UserDispositionLastUpdated = "aduttonadm";
                            break;
                    }
                    break;
            }

            return returnRecord;
        }

        public List<Record> CreateRandomRecords(int numberOfRecords)
        {
            List<Record> recordList = new List<Record>();

            for (int iteration = 0; iteration < numberOfRecords; iteration++)
            {
                recordList.Add(CreateRandomRecord());
            }
            return recordList;
        }

        public RecordRandomizer()
        {
            random = new Random();
        }
    }
}
