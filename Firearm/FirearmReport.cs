﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public enum OrderByColumn { AcquisitionID, BinderNumber };
    public abstract class FirearmReport
    {
        protected DateTime? startDate;
        protected DateTime? endDate;
        protected Store store;
        protected User user;
        protected List<Record> records;
        protected OrderByColumn column;

        public abstract void QueryData();
        public abstract void BuildReport();
    }


}
