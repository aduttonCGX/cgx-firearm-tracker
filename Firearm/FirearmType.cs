﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class FirearmType
    {
        public int TypeID { get; }
        public string TypeDescription { get; }

        public FirearmType(int id, string description)
        {
            TypeID = id;
            TypeDescription = description;
        }
    }
}
