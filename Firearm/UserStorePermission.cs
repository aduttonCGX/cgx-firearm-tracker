﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class UserStorePermission
    {
        private string storeID;
        private bool hasPermission;
        private string storeDescription;        

        public string StoreID { get => storeID; set => storeID = value; }
        public bool HasPermission { get => hasPermission; set => hasPermission = value; }
        public string StoreDescription { get => storeDescription; set => storeDescription = value; }
        public string DisplayDescription { get => $"{storeID} {storeDescription}"; }
        public UserStorePermission(string storeID, bool hasPermission, string storeDescription)
        {
            this.storeID = storeID;
            this.hasPermission = hasPermission;
            this.storeDescription = storeDescription;
        }
    }
}
