﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Utilities;


namespace Firearm
{
    public enum ActivationStatus { Active, Inactive };

    public class User
    {
        private string name;
        private string longName;
        private bool isAdmin;
        private bool isActive;
        private List<Store> stores;
        private List<UserStorePermission> permissions;

        public string Name { get => name; set => name = value; }
        public string LongName { get => longName; set => longName = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
        public List<Store> Stores { get => stores; }
        public bool IsActive { get => isActive; set => isActive = value; }
        public List<UserStorePermission> Permissions { get => permissions; }
        public ActivationStatus Status {
            get
            {
                if (isActive)
                    return ActivationStatus.Active;
                else
                    return ActivationStatus.Inactive; 
            }
        }

        public User(string userString)
        {
            //longName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            longName = userString;
            name = longName.Substring(longName.IndexOf('\\') + 1);
            GetDetailsFromDatabase();
            GetStores();
        }
        
        public User()
        {
            longName = "";
            name = "";
            isActive = true;
            isAdmin = false;
            GetStores();
        }
        private void GetDetailsFromDatabase()
        {
            // Get Active / Admin details
            try
            {
                using (SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString))
                {
                    connection.Open();
                    string query = $@"SELECT [IsAdmin], [IsActive]
                                      FROM [CGESFirearm].[dbo].[users]
                                      WHERE [UserName] = '{name}'";

                    SqlCommand sqlcommand = new SqlCommand(query, connection)
                    {
                        CommandTimeout = 1200
                    };
                    SqlDataReader reader = sqlcommand.ExecuteReader();

                    while (reader.Read())
                    {
                        isAdmin = (bool)reader["IsAdmin"];
                        isActive = (bool)reader["IsActive"];
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }        
        }
        private void GetStores()
        {
            stores = new List<Store>();
            // Get Stores
            string query = "";
            
            // Admins have permissions for all stores
            if (isAdmin)
            {
                query = $@"SELECT [StoreID], [StoreAbbreviation], [StoreDescription] FROM [CGESFirearm].[dbo].[stores] ORDER BY [StoreDescription]";
            }
            else
            {
                query = $@"SELECT 
                            st.[StoreID],
                            st.[StoreAbbreviation],
                            st.[StoreDescription]
                        FROM 
	                        [CGESFirearm].[dbo].[stores] st
	                        JOIN [CGESFirearm].[dbo].[userStorePermissions] usp ON usp.StoreID = st.StoreID
	                        JOIN [CGESFirearm].[dbo].[users] us ON us.UserID = usp.UserID	
                        WHERE
	                        us.UserName = '{name}'
                        ORDER BY 
                            st.[StoreDescription]";
            }
            

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);                    
                    return;
                }
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    stores.Add(new Store((string)row.ItemArray[0], (string)row.ItemArray[1], (string)row.ItemArray[2]));
                }                
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);                
                return;
            }

            // Permissions
            permissions = new List<UserStorePermission>();
            // Get all stores
            query = $@"SELECT 
                        [StoreID],                                
                        [StoreDescription]
                    FROM 
	                    [CGESFirearm].[dbo].[stores]
                    ORDER BY
                        [StoreDescription]";

            // Check database
            connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    return;
                }
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    permissions.Add(new UserStorePermission((string)row.ItemArray[0], false, (string)row.ItemArray[1]));
                }

                // Set true for stores in the store list
                foreach (Store store in stores)
                {
                    permissions.FirstOrDefault(p => p.StoreID == store.StoreID).HasPermission = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }
        }
    }
}
