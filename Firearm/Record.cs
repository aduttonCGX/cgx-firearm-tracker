﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace Firearm
{
    public enum StatusType { Open, Closed, Deleted };
    public class Record
    {
        private readonly string RECORD_KEY = "23ejhe8hoshfwoejnf9";

        private string purchaserFirstName;
        private string purchaserMiddleName;
        private string purchaserLastName;
        private string purchaserAddress1;
        private string purchaserAddress2;
        private string purchaserCity;
        private string purchaserState;
        private string purchaserZip;
        private string purchaserCountry;
        private string primaryIdentificationNumber;        

        public int AcquisitionID { get; set; }
        public int BinderNumber { get; set; }
        public int LineItemNumber { get; set; }
        public string Manufacturer { get; set; }
        public int ManufacturerID { get; set; }
        public string Model { get; set; }
        public int ModelID { get; set; }
        public string SerialNumber { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string CaliberOrGauge { get; set; }
        public int CaliberGaugeID { get; set; }
        public DateTime AcquisitionDate { get; set; }
        public int StoreID { get; set; }
        public string StoreAbbreviation { get; set; }
        public string StoreDescription { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAddress1 { get; set; }
        public string SupplierAddress2 { get; set; }
        public string SupplierCity { get; set; }
        public string SupplierState { get; set; }
        public string SupplierZip { get; set; }
        public string SupplierCountry { get; set; }
        public string SupplierLicenseNumber { get; set; }
        public int SupplierID { get; set; }
        public DateTime DateAcquisitionCreated { get; set; }
        public string UserAcquisitionCreated { get; set; }
        public DateTime DateAcquisitionLastUpdated { get; set; }
        public string UserAcquisitionLastUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string UserDeleted { get; set; }
        public int? DispositionID { get; set; }
        public string PurchaserFirstName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserFirstName))
                    return Encrypt.DecryptString(purchaserFirstName, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserFirstName = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserFirstName = null;
            }
        }
        public string PurchaserMiddleName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserMiddleName))
                    return Encrypt.DecryptString(purchaserMiddleName, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserMiddleName = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserMiddleName = null;
            }
        }
        public string PurchaserLastName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserLastName))
                    return Encrypt.DecryptString(purchaserLastName, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserLastName = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserLastName = null;
            }
        }
        public string PurchaserAddress1
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserAddress1))
                    return Encrypt.DecryptString(purchaserAddress1, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserAddress1 = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserAddress1 = null;
            }
        }
        public string PurchaserAddress2
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserAddress2))
                    return Encrypt.DecryptString(purchaserAddress2, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserAddress2 = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserAddress2 = null;
            }
        }
        public string PurchaserCity
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserCity))
                    return Encrypt.DecryptString(purchaserCity, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserCity = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserCity = null;
            }
        }
        public string PurchaserState
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserState))
                    return Encrypt.DecryptString(purchaserState, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserState = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserState = null;
            }
        }
        public string PurchaserZip
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserZip))
                    return Encrypt.DecryptString(purchaserZip, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserZip = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserZip = null;
            }
        }
        public string PurchaserCountry
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(purchaserCountry))
                    return Encrypt.DecryptString(purchaserCountry, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    purchaserCountry = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    purchaserCountry = null;
            }
        }
        public string PrimaryIdentifierType { get; set; }
        public string PrimaryIdentificationNumber
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(primaryIdentificationNumber))
                    return Encrypt.DecryptString(primaryIdentificationNumber, RECORD_KEY);
                else
                    return null;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    primaryIdentificationNumber = Encrypt.EncryptString(value, RECORD_KEY);
                else
                    primaryIdentificationNumber = null;
            }
        }
        public string Form4473SerialNumber { get; set; }
        public DateTime? DispositionDate { get; set; }
        public DateTime? DateDispositionCreated { get; set; }
        public string UserDispositionCreated { get; set; }
        public DateTime? DateDispositionLastUpdated { get; set; }
        public string UserDispositionLastUpdated { get; set; }
        public StatusType Status
        {
            get
            {
                if (DateDeleted != null)
                    return StatusType.Deleted;
                if (DispositionID == null)
                    return StatusType.Open;
                else
                    return StatusType.Closed;
            }
        }
        public string PurchaserName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(purchaserFirstName) && string.IsNullOrWhiteSpace(purchaserLastName))
                    return null;
                if (string.IsNullOrWhiteSpace(purchaserMiddleName))
                    return $"{PurchaserLastName}, {PurchaserFirstName}";
                else
                    return $"{PurchaserLastName}, {PurchaserFirstName} {PurchaserMiddleName}";
            }
        }
        public string Description { get => $"{Manufacturer} {Model} {Type} {CaliberOrGauge}"; }
        public string PurchaserNameAndAddress
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PurchaserAddress2))
                {
                    return $"{PurchaserName} {Environment.NewLine}{PurchaserAddress1} {Environment.NewLine}{PurchaserCity}, {PurchaserState}  {PurchaserZip}";
                }
                else
                {
                    return $"{PurchaserName} {Environment.NewLine}{PurchaserAddress1} {Environment.NewLine}{PurchaserAddress2} {Environment.NewLine}{PurchaserCity}, {PurchaserState}  {PurchaserZip}";
                }
            }
        }

        public Record()
        {
            AcquisitionID = -1;
            Manufacturer = "";
            ManufacturerID = -1;
            BinderNumber = -1;
            LineItemNumber = -1;
            Model = "";
            ModelID = -1;
            SerialNumber = "";
            Type = "";
            TypeID = -1;
            CaliberOrGauge = "";
            CaliberGaugeID = -1;
            AcquisitionDate = DateTime.Today;
            StoreID = -1;
            StoreAbbreviation = "";
            StoreDescription = "";
            SupplierName = "";
            SupplierAddress1 = "";
            SupplierAddress2 = "";
            SupplierCity = "";
            SupplierState = "";
            SupplierZip = "";
            SupplierCountry = "";
            SupplierLicenseNumber = "";
            SupplierID = -1;
            DateAcquisitionCreated = DateTime.Today;
            UserAcquisitionCreated = "";
            DateAcquisitionLastUpdated = DateTime.Today;
            UserAcquisitionLastUpdated = "";
            DateDeleted = null;
            UserDeleted = null;
            DispositionID = null;
            purchaserFirstName = null;
            purchaserMiddleName = null;
            purchaserLastName = null;
            purchaserAddress1 = null;
            purchaserAddress2 = null;
            purchaserCity = null;
            purchaserState = null;
            purchaserZip = null;
            purchaserCountry = null;
            primaryIdentificationNumber = null;
            Form4473SerialNumber = "";
            DispositionDate = null;
            DateDispositionCreated = null;
            UserDispositionCreated = null;
            DateDispositionLastUpdated = null;
            UserDispositionLastUpdated = null;
            PrimaryIdentifierType = null;
        }

        public bool AddDispositionToDatabase()
        {            
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    User currentUser = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                    string dateCreated = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    SqlCommand cmd = new SqlCommand("sp_AddDisposition", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@DateCreated", dateCreated));
                    cmd.Parameters.Add(new SqlParameter("@UserCreated", currentUser.Name));
                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", AcquisitionID));
                    cmd.Parameters.Add(new SqlParameter("@DispositionDate", DispositionDate));
                    cmd.Parameters.Add(new SqlParameter("@PurchaserFirstName", purchaserFirstName));
                    if (string.IsNullOrEmpty(purchaserMiddleName))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserMiddleName", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserMiddleName", purchaserMiddleName));
                    cmd.Parameters.Add(new SqlParameter("@PurchaserLastName", purchaserLastName));

                    if (string.IsNullOrEmpty(purchaserAddress1))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress1", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress1", purchaserAddress1));
                    if (string.IsNullOrEmpty(purchaserAddress2))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress2", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress2", purchaserAddress2));
                    if (string.IsNullOrEmpty(purchaserCity))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCity", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCity", purchaserCity));
                    if (string.IsNullOrEmpty(purchaserState))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserState", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserState", purchaserState));
                    if (string.IsNullOrEmpty(purchaserZip))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserZip", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserZip", purchaserZip));
                    if (string.IsNullOrEmpty(purchaserCountry))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCountry", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCountry", purchaserCountry));
                    if (string.IsNullOrEmpty(primaryIdentificationNumber))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLicenseNumber", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLicenseNumber", primaryIdentificationNumber));
                    if (string.IsNullOrEmpty(Form4473SerialNumber))
                        cmd.Parameters.Add(new SqlParameter("@Form4473SerialNumber", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Form4473SerialNumber", Form4473SerialNumber));
                    if (string.IsNullOrEmpty(Form4473SerialNumber))
                        cmd.Parameters.Add(new SqlParameter("@PrimaryIdentifierType", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PrimaryIdentifierType", PrimaryIdentifierType));

                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", dateCreated));
                    cmd.Parameters.Add(new SqlParameter("@UserUpdated", currentUser.Name));
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }
        }

        public void UpdateRecordInDatabase()
        {
            // Update object properties
            User user = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            UserAcquisitionLastUpdated = user.Name;
            DateAcquisitionLastUpdated = DateTime.Now;

            // Update database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = "sp_UpdateAcquisition",
                        CommandType = CommandType.StoredProcedure,
                        Connection = conn
                    };

                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", AcquisitionID));
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerID", ManufacturerID));
                    cmd.Parameters.Add(new SqlParameter("@ModelID", ModelID));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", SerialNumber));
                    cmd.Parameters.Add(new SqlParameter("@TypeID", TypeID));
                    cmd.Parameters.Add(new SqlParameter("@CaliberGaugeID", CaliberGaugeID));
                    cmd.Parameters.Add(new SqlParameter("@DateAcquired", AcquisitionDate));
                    cmd.Parameters.Add(new SqlParameter("@StoreID", StoreID));
                    cmd.Parameters.Add(new SqlParameter("@SupplierID", SupplierID));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", DateAcquisitionLastUpdated));
                    cmd.Parameters.Add(new SqlParameter("@UserUpdated", UserAcquisitionLastUpdated));
                    cmd.Parameters.Add(new SqlParameter("@BinderNumber", BinderNumber));
                    cmd.Parameters.Add(new SqlParameter("@LineItemNumber", LineItemNumber));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }

            // Update Disposition, if one exists
            if (DispositionID == null)
            {
                return;
            }

            // Update object properties
            UserDispositionLastUpdated = user.Name;
            DateDispositionLastUpdated = DateAcquisitionLastUpdated;

            // Update database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = "sp_UpdateDisposition",
                        CommandType = CommandType.StoredProcedure,
                        Connection = conn
                    };

                    cmd.Parameters.Add(new SqlParameter("@DispositionID", DispositionID));
                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", AcquisitionID));
                    cmd.Parameters.Add(new SqlParameter("@DispositionDate", DispositionDate.Value.ToString("MM/dd/yyyy HH:mm:ss")));
                    if (string.IsNullOrWhiteSpace(purchaserFirstName))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserFirstName", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserFirstName", purchaserFirstName));
                    if (string.IsNullOrWhiteSpace(purchaserMiddleName))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserMiddleName", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserMiddleName", purchaserMiddleName));
                    if (string.IsNullOrWhiteSpace(purchaserLastName))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLastName", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLastName", purchaserLastName));
                    if (string.IsNullOrWhiteSpace(purchaserAddress1))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress1", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress1", purchaserAddress1));
                    if (string.IsNullOrWhiteSpace(purchaserAddress2))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress2", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserAddress2", purchaserAddress2));
                    if (string.IsNullOrWhiteSpace(purchaserCity))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCity", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCity", purchaserCity));
                    if (string.IsNullOrWhiteSpace(purchaserState))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserState", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserState", purchaserState));
                    if (string.IsNullOrWhiteSpace(purchaserZip))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserZip", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserZip", purchaserZip));
                    if (string.IsNullOrWhiteSpace(purchaserCountry))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCountry", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserCountry", purchaserCountry));
                    if (string.IsNullOrWhiteSpace(PrimaryIdentifierType))
                        cmd.Parameters.Add(new SqlParameter("@PrimaryIdentifierType", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PrimaryIdentifierType", PrimaryIdentifierType));
                    if (string.IsNullOrWhiteSpace(primaryIdentificationNumber))
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLicenseNumber", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@PurchaserLicenseNumber", primaryIdentificationNumber));
                    cmd.Parameters.Add(new SqlParameter("@Form4473SerialNumber", Form4473SerialNumber));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", DateDispositionLastUpdated.Value.ToString("MM/dd/yyyy HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@UserUpdated", UserDispositionLastUpdated));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }
        }
        public void DeleteDisposition()
        {
            if (DispositionID == null)
            {
                return;
            }

            // Update database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = "sp_DeleteDisposition",
                        CommandType = CommandType.StoredProcedure,
                        Connection = conn
                    };

                    User user = new User();
                    cmd.Parameters.Add(new SqlParameter("@DispositionID", DispositionID));
                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", AcquisitionID));
                    cmd.Parameters.Add(new SqlParameter("@UserDeleted", user.Name));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }

            // Clear object properties
            DispositionID = null;
            purchaserFirstName = null;
            purchaserMiddleName = null;
            purchaserLastName = null;
            purchaserAddress1 = null;
            purchaserAddress2 = null;
            purchaserCity = null;
            purchaserState = null;
            purchaserZip = null;
            purchaserCountry = null;
            PrimaryIdentifierType = null;
            primaryIdentificationNumber = null;
            Form4473SerialNumber = "";
            DispositionDate = null;
            DateDispositionCreated = null;
            UserDispositionCreated = null;
            DateDispositionLastUpdated = null;
            UserDispositionLastUpdated = null;
        }
        public void DeleteAcquisition()
        {
            // Update object properties
            User user = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            UserDeleted = user.Name;
            DateDeleted = DateTime.Now;

            // Update database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand()
                    {
                        CommandText = "sp_DeleteAcquisition",
                        CommandType = CommandType.StoredProcedure,
                        Connection = conn
                    };

                    cmd.Parameters.Add(new SqlParameter("@DateDeleted", DateDeleted.Value));
                    cmd.Parameters.Add(new SqlParameter("@UserDeleted", UserDeleted));
                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", AcquisitionID));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }
        }
        public bool AcquisitionIsCurrent()
        {
            string query = $@"SELECT
	                            ManufacturerID,
	                            ModelID,
	                            SerialNumber,
	                            TypeID,
	                            CaliberGaugeID,
	                            DateAcquired,
	                            StoreID,
	                            SupplierID	
                            FROM 
	                            acquisitionDetails
                            WHERE        
	                            (dbo.acquisitionDetails.DateUpdated =
		                            (SELECT 
			                            MAX(DateUpdated)
		                            FROM
			                            dbo.acquisitionDetails
		                            WHERE
			                            (AcquisitionID = {AcquisitionID})))
	                            AND AcquisitionID = {AcquisitionID}";
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Compare the data                
                if (ManufacturerID == (int)ds.Tables[0].Rows[0].ItemArray[0]
                    && ModelID == (int)ds.Tables[0].Rows[0].ItemArray[1]
                    && SerialNumber == (string)ds.Tables[0].Rows[0].ItemArray[2]
                    && TypeID == (int)ds.Tables[0].Rows[0].ItemArray[3]
                    && CaliberGaugeID == (int)ds.Tables[0].Rows[0].ItemArray[4]
                    && AcquisitionDate == (DateTime)ds.Tables[0].Rows[0].ItemArray[5]
                    && StoreID == (int)ds.Tables[0].Rows[0].ItemArray[6]
                    && SupplierID == (int)ds.Tables[0].Rows[0].ItemArray[7])
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }
        }
        public void ImportPurchaserFirstName(string encryptedString)
        {
            purchaserFirstName = encryptedString;
        }
        public void ImportPurchaserMiddleName(string encryptedString)
        {
            purchaserMiddleName = encryptedString;
        }
        public void ImportPurchaserLastName(string encryptedString)
        {
            purchaserLastName = encryptedString;
        }
        public void ImportPurchaserAddress1(string encryptedString)
        {
            purchaserAddress1 = encryptedString;
        }
        public void ImportPurchaserAddress2(string encryptedString)
        {
            purchaserAddress2 = encryptedString;
        }
        public void ImportPurchaserCity(string encryptedString)
        {
            purchaserCity = encryptedString;
        }
        public void ImportPurchaserState(string encryptedString)
        {
            purchaserState = encryptedString;
        }
        public void ImportPurchaserZip(string encryptedString)
        {
            purchaserZip = encryptedString;
        }
        public void ImportPurchaserCountry(string encryptedString)
        {
            purchaserCountry = encryptedString;
        }
        public void ImportPrimaryIdentificationNumber(string encryptedString)
        {
            primaryIdentificationNumber = encryptedString;
        }        
    }
}
