﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Supplier
    {
        public int SupplierID { get; }
        public string SupplierDescription { get; }
        public string Address1 { get; }
        public string Address2 { get; }
        public string City { get; }
        public string State { get; }
        public string Zip { get; }
        public string Country { get; }
        public string LicenseNumber { get; }

        public Supplier(int supplierID, string supplierDescription, string address1 = "", string address2 = "", string city = "", string state = "", string zip = "", string country = "", string licenseNumber = "")
        {
            SupplierID = supplierID;
            SupplierDescription = supplierDescription;
            Address1 = address1;
            Address2 = address2;
            City = city;
            State = state;
            Zip = zip;
            Country = country;
            LicenseNumber = licenseNumber;
        }
    }
}
