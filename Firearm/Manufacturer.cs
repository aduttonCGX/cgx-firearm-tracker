﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Firearm
{
    public class Manufacturer
    {   
        public int ManufacturerID { get; }
        public string ManufacturerDescription { get; }

        public Manufacturer(int id, string description)
        {
            ManufacturerID = id;
            ManufacturerDescription = description;
        }
    }
}
