﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddSupplierWindow.xaml
    /// </summary>
    public partial class AddSupplierWindow : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        
        public AddSupplierWindow()
        {
            InitializeComponent();
            txtSupplierName.Focus();
            Title = lblTitle.Text;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void AddToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtSupplierName.Text))
            {
                MBWindow.Show("Must include a supplier name.", "Error", MessageBoxButton.OK);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtLicenseNumber.Text))
            {
                if (string.IsNullOrWhiteSpace(txtAddress1.Text) || string.IsNullOrWhiteSpace(txtCity.Text) || string.IsNullOrWhiteSpace(txtState.Text) || string.IsNullOrWhiteSpace(txtZip.Text))
                {
                    MBWindow.Show("Must include a complete address or license number.", "Error", MessageBoxButton.OK);
                    return;
                }
            }

            // Add to database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_AddSupplier", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SupplierDescription", txtSupplierName.Text));
                    if (string.IsNullOrWhiteSpace(txtAddress1.Text))
                        cmd.Parameters.Add(new SqlParameter("@Address1", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Address1", txtAddress1.Text));
                    if (string.IsNullOrWhiteSpace(txtAddress2.Text))
                        cmd.Parameters.Add(new SqlParameter("@Address2", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Address2", txtAddress2.Text));
                    if (string.IsNullOrWhiteSpace(txtCity.Text))
                        cmd.Parameters.Add(new SqlParameter("@City", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@City", txtCity.Text));
                    if (string.IsNullOrWhiteSpace(txtState.Text))
                        cmd.Parameters.Add(new SqlParameter("@State", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@State", txtState.Text));
                    if (string.IsNullOrWhiteSpace(txtZip.Text))
                        cmd.Parameters.Add(new SqlParameter("@Zip", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Zip", txtZip.Text));
                    if (string.IsNullOrWhiteSpace(txtCountry.Text))
                        cmd.Parameters.Add(new SqlParameter("@Country", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@Country", txtCountry.Text));
                    if (string.IsNullOrWhiteSpace(txtLicenseNumber.Text))
                        cmd.Parameters.Add(new SqlParameter("@LicenseNumber", DBNull.Value));
                    else
                        cmd.Parameters.Add(new SqlParameter("@LicenseNumber", txtLicenseNumber.Text));

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            finally
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            AddToDatabase();
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            this.Close();
        }
        private void TxtLicenseNumber_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    AddToDatabase();
                    break;
                default:
                    break;
            }
        }
    }
}
