﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddEditAcquisitionWindow.xaml
    /// </summary>
    public partial class AddEditAcquisitionWindow : Window
    {
        private bool buttonDepressed = false;
        private string storeID;
        public WindowResult Result;
        private List<Store> stores;
        private List<Manufacturer> manufacturers;
        private List<Model> models;        
        private List<FirearmType> types;
        private List<Caliber> calibers;
        private List<Supplier> suppliers;
        private User currentUser;
        private Record record;
        private bool editMode = false;
        private List<string> serialNumbers;

        // Open in "Add Acquisition" mode
        public AddEditAcquisitionWindow(string storeID, List<Store> usersStoreList)
        {
            InitializeComponent();
            txtSerialNumber.Focus();
            dpAcquisitionDate.SelectedDate = DateTime.Today;
            currentUser = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            this.storeID = storeID;
            editMode = false;
            Title = lblTitle.Text;

            // Initialize comboboxes
            PopulateManufacturerComboBox();            
            PopulateTypeComboBox();
            PopulateCaliberGaugeComboBox();
            PopulateSupplierComboBox();

            // Populate store combobox
            cboxStore.ItemsSource = usersStoreList;
            cboxStore.DisplayMemberPath = "StoreDescription";
            cboxStore.SelectedValuePath = "StoreID";
            cboxStore.SelectedValue = storeID;

            // Prepare for serial number validation
            GetUsedSerialNumbers();
            lblSerialNumberValidationText.Visibility = Visibility.Hidden;

            // Can't choose future dates
            dpAcquisitionDate.DisplayDateEnd = DateTime.Now;
        }

        // Open in "Edit Acquisition" mode
        public AddEditAcquisitionWindow(Record record, List<Store> usersStoreList)
        {
            InitializeComponent();
            txtSerialNumber.Focus();
            currentUser = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            this.record = record;
            editMode = true;

            lblTitle.Text = $"Edit Acquisition #{record.AcquisitionID}";
            Title = lblTitle.Text;

            // Prepare for serial number validation
            GetUsedSerialNumbers();
            lblSerialNumberValidationText.Visibility = Visibility.Hidden;

            // Initialize comboboxes
            PopulateManufacturerComboBox();            
            PopulateTypeComboBox();
            PopulateCaliberGaugeComboBox();
            PopulateSupplierComboBox();

            // Populate store combobox
            cboxStore.ItemsSource = usersStoreList;
            cboxStore.DisplayMemberPath = "StoreDescription";
            cboxStore.SelectedValuePath = "StoreID";
            cboxStore.SelectedValue = storeID;

            // Load current data
            PopulateFields();

            // Can't choose future dates
            dpAcquisitionDate.DisplayDateEnd = DateTime.Now;
        }
        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);
                    
                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void PopulateStoreNumberComboBox()
        {
            stores = new List<Store>();
            string query = @"SELECT StoreID, StoreAbbreviation, StoreDescription FROM [CGESFirearm].[dbo].[stores] ORDER BY StoreDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    stores.Add(new Store((string)row.ItemArray[0], (string)row.ItemArray[1], (string)row.ItemArray[2]));
                }

                // Bind to the combobox
                cboxStore.ItemsSource = stores;
                cboxStore.DisplayMemberPath = "StoreDescription";
                cboxStore.SelectedValuePath = "StoreID";
                cboxStore.SelectedValue = storeID;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void PopulateManufacturerComboBox()
        {
            manufacturers = new List<Manufacturer>();
            string query = @"SELECT ManufacturerID, ManufacturerDescription FROM [CGESFirearm].[dbo].[manufacturers] ORDER BY ManufacturerDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    manufacturers.Add(new Manufacturer((int)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxManufacturer.ItemsSource = manufacturers;
                cboxManufacturer.DisplayMemberPath = "ManufacturerDescription";
                cboxManufacturer.SelectedValuePath = "ManufacturerID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void PopulateModelComboBox()
        {
            models = new List<Model>();
            string query = $"SELECT ModelID, ModelDescription FROM [CGESFirearm].[dbo].[models] WHERE ManufacturerID = {cboxManufacturer.SelectedValue} ORDER BY ModelDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    models.Add(new Model((int)row.ItemArray[0], (int)cboxManufacturer.SelectedValue, (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxModel.ItemsSource = models;
                cboxModel.DisplayMemberPath = "ModelDescription";
                cboxModel.SelectedValuePath = "ModelID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            cboxModel.IsEnabled = true;
            btnNewModel.IsEnabled = true;
        }
        private void PopulateTypeComboBox()
        {
            types = new List<FirearmType>();
            string query = @"SELECT TypeID, TypeDescription FROM [CGESFirearm].[dbo].[types] ORDER BY TypeDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    types.Add(new FirearmType((int)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxType.ItemsSource = types;
                cboxType.DisplayMemberPath = "TypeDescription";
                cboxType.SelectedValuePath = "TypeID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void PopulateCaliberGaugeComboBox()
        {
            calibers = new List<Caliber>();
            string query = @"SELECT CaliberGaugeID, CaliberGaugeDescription FROM [CGESFirearm].[dbo].[caliberGauge] ORDER BY CaliberGaugeDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    calibers.Add(new Caliber((int)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxCaliber.ItemsSource = calibers;
                cboxCaliber.DisplayMemberPath = "CaliberDescription";
                cboxCaliber.SelectedValuePath = "CaliberID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void PopulateSupplierComboBox()
        {
            suppliers = new List<Supplier>();
            string query = @"SELECT SupplierID, SupplierDescription FROM [CGESFirearm].[dbo].[suppliers] ORDER BY SupplierDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    suppliers.Add(new Supplier((int)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxSupplier.ItemsSource = suppliers;
                cboxSupplier.DisplayMemberPath = "SupplierDescription";
                cboxSupplier.SelectedValuePath = "SupplierID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void PopulateFields()
        {
            cboxStore.SelectedValue = record.StoreID;
            txtBinderNumber.Text = record.BinderNumber.ToString();
            txtLineItemNumber.Text = record.LineItemNumber.ToString();
            dpAcquisitionDate.SelectedDate = record.AcquisitionDate;
            txtSerialNumber.Text = record.SerialNumber;
            cboxManufacturer.SelectedValue = record.ManufacturerID;
            PopulateModelComboBox();
            cboxModel.SelectedValue = record.ModelID;
            cboxType.SelectedValue = record.TypeID;
            cboxCaliber.SelectedValue = record.CaliberGaugeID;
            cboxSupplier.SelectedValue = record.SupplierID;
        }
        private void UpdateDatabase()
        {
            if (cboxStore.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a store number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtSerialNumber.Text))
            {
                MBWindow.Show("Must provide a serial number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (lblSerialNumberValidationText.Visibility == Visibility.Visible)
            {
                MBWindow.Show("Must provide a unique serial number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtBinderNumber.Text))
            {
                MBWindow.Show("Must provide a binder number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (!txtBinderNumber.Text.All(Char.IsDigit))
            {
                MBWindow.Show("Binder number must be numeric.", "Error", MessageBoxButton.OK);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtLineItemNumber.Text))
            {
                MBWindow.Show("Must provide a line item number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (!txtLineItemNumber.Text.All(Char.IsDigit))
            {
                MBWindow.Show("Line item number must be numeric.", "Error", MessageBoxButton.OK);
                return;
            }
            
            if (cboxManufacturer.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a manufacturer.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxModel.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a model.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxType.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a type.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxCaliber.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a caliber or gauge.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxSupplier.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a supplier.", "Error", MessageBoxButton.OK);
                return;
            }

            // Update database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string dateUpdated = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    SqlCommand cmd = new SqlCommand("sp_UpdateAcquisition", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@AcquisitionID", record.AcquisitionID));                    
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerID", cboxManufacturer.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@ModelID", cboxModel.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", txtSerialNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@TypeID", cboxType.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@CaliberGaugeID", cboxCaliber.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@DateAcquired", dpAcquisitionDate.SelectedDate));
                    cmd.Parameters.Add(new SqlParameter("@StoreID", cboxStore.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@SupplierID", cboxSupplier.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", dateUpdated));
                    cmd.Parameters.Add(new SqlParameter("@UserUpdated", currentUser.Name));
                    cmd.Parameters.Add(new SqlParameter("@BinderNumber", txtBinderNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@LineItemNumber", txtLineItemNumber.Text));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }           
            
            Result = WindowResult.OK;
            MBWindow.Show($"Item updated successfully.", "Notice", MessageBoxButton.OK);
            Close();            
        }
        private void AddToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtSerialNumber.Text))
            {
                MBWindow.Show("Must provide a serial number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (lblSerialNumberValidationText.Visibility == Visibility.Visible)
            {
                MBWindow.Show("Must provide a unique serial number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxStore.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a store number.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxManufacturer.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a manufacturer.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxModel.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a model.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxType.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a type.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxCaliber.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a caliber or gauge.", "Error", MessageBoxButton.OK);
                return;
            }
            if (cboxSupplier.SelectedIndex == -1)
            {
                MBWindow.Show("Must choose a supplier.", "Error", MessageBoxButton.OK);
                return;
            }
            
            // Add to database
            try
            {                
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string dateCreated = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    SqlCommand cmd = new SqlCommand("sp_AddAcquisition", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add(new SqlParameter("@DateCreated", dateCreated));
                    cmd.Parameters.Add(new SqlParameter("@UserCreated", currentUser.Name));
                    cmd.Parameters.Add(new SqlParameter("@ManufacturerID", cboxManufacturer.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@ModelID", cboxModel.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@SerialNumber", txtSerialNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@TypeID", cboxType.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@CaliberGaugeID", cboxCaliber.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@DateAcquired", dpAcquisitionDate.SelectedDate));
                    cmd.Parameters.Add(new SqlParameter("@StoreID", cboxStore.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@SupplierID", cboxSupplier.SelectedValue));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", dateCreated));
                    cmd.Parameters.Add(new SqlParameter("@UserUpdated", currentUser.Name));
                    cmd.Parameters.Add(new SqlParameter("@BinderNumber", txtBinderNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@LineItemNumber", txtLineItemNumber.Text));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            Result = WindowResult.OK;
            MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
            Close();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (editMode)
            {
                UpdateDatabase();
            }
            else
            {
                AddToDatabase();
            }            
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            this.Close();
        }        
        private void BtnNewManufacturer_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.Manufacturer);
            addNewDetailWindow.ShowDialog();

            if (addNewDetailWindow.Result == WindowResult.OK)
            {
                PopulateManufacturerComboBox();
            }
        }
        private void BtnNewModel_Click(object sender, RoutedEventArgs e)
        {
            AddNewModelWindow addNewModelWindow = new AddNewModelWindow(cboxManufacturer.SelectedIndex);
            addNewModelWindow.ShowDialog();

            if (addNewModelWindow.Result == WindowResult.OK)
            {
                PopulateModelComboBox();
            }
        }
        private void BtnNewType_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.Type);
            addNewDetailWindow.ShowDialog();

            if (addNewDetailWindow.Result == WindowResult.OK)
            {
                PopulateTypeComboBox();
            }
        }
        private void BtnNewCaliber_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.CaliberGauge);
            addNewDetailWindow.ShowDialog();

            if (addNewDetailWindow.Result == WindowResult.OK)
            {
                PopulateCaliberGaugeComboBox();
            }
        }
        private void BtnNewSupplier_Click(object sender, RoutedEventArgs e)
        {
            AddSupplierWindow addSupplierWindow = new AddSupplierWindow();
            addSupplierWindow.ShowDialog();

            if (addSupplierWindow.Result == WindowResult.OK)
            {
                PopulateSupplierComboBox();
            }
        }
        private void CboxManufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboxManufacturer.SelectedIndex == -1)
            {
                cboxModel.SelectedIndex = -1;
                cboxModel.IsEnabled = false;
                btnNewModel.IsEnabled = false;
                cboxType.SelectedIndex = -1;
                cboxType.IsEnabled = false;
                btnNewType.IsEnabled = false;
                cboxCaliber.SelectedIndex = -1;
                cboxCaliber.IsEnabled = false;
                btnNewCaliber.IsEnabled = false;
                return;
            }
            else
            {
                if (editMode == false)
                    PopulateModelComboBox();
                cboxModel.IsEnabled = true;
                btnNewModel.IsEnabled = true;
                cboxType.IsEnabled = true;
                btnNewType.IsEnabled = true;
                cboxCaliber.IsEnabled = true;
                btnNewCaliber.IsEnabled = true;
            }
        }
        private void CboxManufacturer_DropDownClosed(object sender, EventArgs e)
        {
            if (cboxManufacturer.SelectedIndex == -1)
            {
                return;
            }
            else
            {
                PopulateModelComboBox();
            }            
        }
        private void GetUsedSerialNumbers()
        {
            serialNumbers = new List<string>();
            string whereClause = editMode ? $"AcquisitionID <> '{record.AcquisitionID}' AND" : "";
            string query = $@"SELECT 
                                [SerialNumber]      
                            FROM 
	                            [CGESFirearm].[dbo].[v_AcquisitionsFullDetails]
                            WHERE {whereClause} DateDeleted IS NULL";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    serialNumbers.Add(row.ItemArray[0].ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void TxtSerialNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSerialNumber.Text))
            {
                lblSerialNumberValidationText.Visibility = Visibility.Hidden;
                return;
            }

            if (serialNumbers.Contains(txtSerialNumber.Text))
            {
                lblSerialNumberValidationText.Visibility = Visibility.Visible;
            }
            else
            {
                lblSerialNumberValidationText.Visibility = Visibility.Hidden;                
            }
        }
    }
}
