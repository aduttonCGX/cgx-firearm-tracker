﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddStoreWindow.xaml
    /// </summary>
    public partial class AddStoreWindow : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        private List<string> storeNumbers;
        private List<string> storeAbbreviations;
        
        public AddStoreWindow()
        {
            InitializeComponent();
            GetStoreData();
            lblStoreAbbreviationValidationText.Text = "";
            lblStoreNumberValidationText.Text = "";
            txtStoreNumber.Focus();
            Title = lblTitle.Text;
        }

        private void GetStoreData()
        {
            storeNumbers = new List<string>();
            storeAbbreviations = new List<string>();

            string query = @"SELECT StoreID, StoreAbbreviation FROM [CGESFirearm].[dbo].[stores]";

            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection);

                cmd.CommandTimeout = 600;

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    storeNumbers.Add((string)row.ItemArray[0]);
                    storeAbbreviations.Add((string)row.ItemArray[1]);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }
        }
        private void AddToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtStoreAbbreviation.Text) || string.IsNullOrWhiteSpace(txtStoreNumber.Text) || string.IsNullOrWhiteSpace(txtStoreDescription.Text))
            {
                MBWindow.Show("All fields are required.", "Error", MessageBoxButton.OK);
                return;
            }

            if (lblStoreAbbreviationValidationText.Text != "" || lblStoreNumberValidationText.Text != "")
            {
                MBWindow.Show("Please correct validation errors.", "Error", MessageBoxButton.OK);
                return;
            }

            if (txtStoreNumber.Text.Length < 3)
            {
                MBWindow.Show("Store number must be three digits.", "Error", MessageBoxButton.OK);
                return;
            }
            if (txtStoreAbbreviation.Text.Length < 3)
            {
                MBWindow.Show("Store abbreviation must be three characters.", "Error", MessageBoxButton.OK);
                return;
            }

            // Add to database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_AddStore", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@StoreID", txtStoreNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@StoreAbbreviation", txtStoreAbbreviation.Text));
                    cmd.Parameters.Add(new SqlParameter("@StoreDescription", txtStoreDescription.Text));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            finally
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
        }
        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            AddToDatabase();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }

        private void txtStoreNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtStoreNumber.Text))
            {
                lblStoreNumberValidationText.Text = "";
                return;
            }               

            int myInt;
            if (int.TryParse(txtStoreNumber.Text, out myInt))
            {
                if (storeNumbers.Contains(txtStoreNumber.Text))
                {
                    lblStoreNumberValidationText.Text = "Store number already exists.";
                }
                else
                {
                    lblStoreNumberValidationText.Text = "";
                }
            }
            else
            {
                lblStoreNumberValidationText.Text = "Numeric characters only.";
            }
        }

        private void txtStoreAbbreviation_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtStoreAbbreviation.Text))
            {
                lblStoreAbbreviationValidationText.Text = "";
                return;
            }

            if (storeAbbreviations.Contains(txtStoreAbbreviation.Text))
            {
                lblStoreAbbreviationValidationText.Text = "Store abbreviation already exists.";
            }
            else
            {
                lblStoreAbbreviationValidationText.Text = "";
            }            
        }

        private void TxtStoreDescription_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    AddToDatabase();
                    break;
                default:
                    break;
            }
        }
    }
}
