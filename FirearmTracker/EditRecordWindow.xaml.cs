﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Firearm;
using Utilities;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for EditRecordWindow.xaml
    /// </summary>
    public partial class EditRecordWindow : Window
    {
        private bool buttonDepressed;
        private User currentUser;

        public EditRecordWindow(User currentUser)
        {
            this.currentUser = currentUser;
            InitializeComponent();
            txtRecordNumber.Focus();
        }

        #region Common Controls
        private void Btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void Btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        #endregion

        private void RbAcquisition_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RbDisposition_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnOpenRecord_Click(object sender, RoutedEventArgs e)
        {
            OpenRecord();
        }

        private void TxtRecordNumber_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    OpenRecord();
                    break;
                default:
                    break;
            }
        }

        private void OpenRecord()
        {
            // Check that a valid number has been entered
            if (string.IsNullOrWhiteSpace(txtRecordNumber.Text))
            {
                MBWindow.Show("No record number provided.", "Invalid Record Number", MessageBoxButton.OK);
                return;
            }
            if (!txtRecordNumber.Text.All(Char.IsDigit))
            {
                MBWindow.Show("Record number must be numerical.", "Invalid Record Number", MessageBoxButton.OK);
                return;
            }

            // Retrieve record from database
            Record record;
            if (rbAcquisition.IsChecked.Value)
            {
                if (!GetRecordByAcquisitionNumber(txtRecordNumber.Text, out record))
                {
                    MBWindow.Show("No record found.", "Record Lookup Error", MessageBoxButton.OK);
                    return;
                }
            }
            else
            {
                if (!GetRecordByDispositionNumber(txtRecordNumber.Text, out record))
                {
                    MBWindow.Show("No record found.", "Record Lookup Error", MessageBoxButton.OK);
                    return;
                }
            }

            // Open editor window depending on type
            if (rbAcquisition.IsChecked.Value)
            {
                AddEditAcquisitionWindow acquisitionWindow = new AddEditAcquisitionWindow(record, currentUser.Stores);
                acquisitionWindow.ShowDialog();
            }
            else
            {
                AddEditDispositionWindow dispositionWindow = new AddEditDispositionWindow(record);
                dispositionWindow.ShowDialog();
            }
        }

        private bool GetRecordByAcquisitionNumber(string acquisitionNumber, out Record record)
        {
            Record returnRecord = new Record();
            string query = $@"SELECT TOP 1 * FROM [CGESFirearm].[dbo].[v_RecordsFullDetails] WHERE AcquisitionID = {acquisitionNumber}";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Return if nothing is found
                if (ds.Tables[0].Rows.Count == 0)
                {
                    record = new Record();
                    return false;
                }

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    returnRecord = new Record
                    {
                        AcquisitionID = (int)row.ItemArray[0],
                        DateAcquisitionCreated = (DateTime)row.ItemArray[1],
                        UserAcquisitionCreated = (string)row.ItemArray[2],
                        DateDispositionCreated = (row.IsNull(3)) ? (DateTime?)null : (DateTime)row.ItemArray[3],
                        UserDispositionCreated = (row.IsNull(4)) ? null : (string)row.ItemArray[4],
                        DateDeleted = (row.IsNull(5)) ? (DateTime?)null : (DateTime)row.ItemArray[5],
                        UserDeleted = (row.IsNull(6)) ? null : (string)row.ItemArray[6],
                        DateAcquisitionLastUpdated = (DateTime)row.ItemArray[7],
                        UserAcquisitionLastUpdated = (string)row.ItemArray[8],
                        DateDispositionLastUpdated = (row.IsNull(9)) ? (DateTime?)null : (DateTime)row.ItemArray[9],
                        UserDispositionLastUpdated = (row.IsNull(10)) ? null : (string)row.ItemArray[10],
                        SerialNumber = (string)row.ItemArray[11],
                        ManufacturerID = (int)row.ItemArray[12],
                        Manufacturer = (string)row.ItemArray[13],
                        ModelID = (int)row.ItemArray[14],
                        Model = (string)row.ItemArray[15],
                        TypeID = (int)row.ItemArray[16],
                        Type = (string)row.ItemArray[17],
                        CaliberGaugeID = (int)row.ItemArray[18],
                        CaliberOrGauge = (string)row.ItemArray[19],
                        AcquisitionDate = (DateTime)row.ItemArray[20],
                        StoreID = int.Parse(row.ItemArray[21].ToString()),
                        StoreAbbreviation = (string)row.ItemArray[22],
                        StoreDescription = (string)row.ItemArray[23],
                        SupplierID = (int)row.ItemArray[24],
                        SupplierName = (string)row.ItemArray[25],
                        SupplierAddress1 = (row.IsNull(26)) ? null : (string)row.ItemArray[26],
                        SupplierAddress2 = (row.IsNull(27)) ? null : (string)row.ItemArray[27],
                        SupplierCity = (row.IsNull(28)) ? null : (string)row.ItemArray[28],
                        SupplierState = (row.IsNull(29)) ? null : (string)row.ItemArray[29],
                        SupplierZip = (row.IsNull(30)) ? null : (string)row.ItemArray[30],
                        SupplierCountry = (row.IsNull(31)) ? null : (string)row.ItemArray[31],
                        SupplierLicenseNumber = (row.IsNull(32)) ? null : (string)row.ItemArray[32],
                        DispositionID = (row.IsNull(33)) ? (int?)null : (int)row.ItemArray[33],
                        DispositionDate = (row.IsNull(34)) ? (DateTime?)null : (DateTime)row.ItemArray[34]
                    };

                    if (!row.IsNull(35))
                        returnRecord.ImportPurchaserFirstName((string)row.ItemArray[35]);
                    if (!row.IsNull(36))
                        returnRecord.ImportPurchaserMiddleName((string)row.ItemArray[36]);
                    if (!row.IsNull(37))
                        returnRecord.ImportPurchaserLastName((string)row.ItemArray[37]);
                    if (!row.IsNull(38))
                        returnRecord.ImportPurchaserAddress1((string)row.ItemArray[38]);
                    if (!row.IsNull(39))
                        returnRecord.ImportPurchaserAddress2((string)row.ItemArray[39]);
                    if (!row.IsNull(40))
                        returnRecord.ImportPurchaserCity((string)row.ItemArray[40]);
                    if (!row.IsNull(41))
                        returnRecord.ImportPurchaserState((string)row.ItemArray[41]);
                    if (!row.IsNull(42))
                        returnRecord.ImportPurchaserZip((string)row.ItemArray[42]);
                    if (!row.IsNull(43))
                        returnRecord.ImportPurchaserCountry((string)row.ItemArray[43]);
                    if (!row.IsNull(44))
                        returnRecord.ImportPrimaryIdentificationNumber((string)row.ItemArray[44]);
                    returnRecord.Form4473SerialNumber = (row.IsNull(45)) ? null : (string)row.ItemArray[45];

                    if (int.TryParse((string)row.ItemArray[46], out int tempBinderNumber))
                    {
                        returnRecord.BinderNumber = tempBinderNumber;
                    }
                    else
                    {
                        returnRecord.BinderNumber = 0;
                    }
                    if (int.TryParse((string)row.ItemArray[47], out int tempLineItemNumber))
                    {
                        returnRecord.LineItemNumber = tempLineItemNumber;
                    }
                    else
                    {
                        returnRecord.LineItemNumber = 0;
                    }
                    returnRecord.PrimaryIdentifierType = (row.IsNull(48)) ? null : (string)row.ItemArray[48];
                }
                record = returnRecord;
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                record = new Record();
                return false;
            }
        }

        private bool GetRecordByDispositionNumber(string dipositionNumber, out Record record)
        {
            Record returnRecord = new Record();
            string query = $@"SELECT TOP 1 * FROM [CGESFirearm].[dbo].[v_RecordsFullDetails] WHERE DispositionID = {dipositionNumber}";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Return if nothing is found
                if (ds.Tables[0].Rows.Count == 0)
                {
                    record = new Record();
                    return false;
                }

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    returnRecord = new Record
                    {
                        AcquisitionID = (int)row.ItemArray[0],
                        DateAcquisitionCreated = (DateTime)row.ItemArray[1],
                        UserAcquisitionCreated = (string)row.ItemArray[2],
                        DateDispositionCreated = (row.IsNull(3)) ? (DateTime?)null : (DateTime)row.ItemArray[3],
                        UserDispositionCreated = (row.IsNull(4)) ? null : (string)row.ItemArray[4],
                        DateDeleted = (row.IsNull(5)) ? (DateTime?)null : (DateTime)row.ItemArray[5],
                        UserDeleted = (row.IsNull(6)) ? null : (string)row.ItemArray[6],
                        DateAcquisitionLastUpdated = (DateTime)row.ItemArray[7],
                        UserAcquisitionLastUpdated = (string)row.ItemArray[8],
                        DateDispositionLastUpdated = (row.IsNull(9)) ? (DateTime?)null : (DateTime)row.ItemArray[9],
                        UserDispositionLastUpdated = (row.IsNull(10)) ? null : (string)row.ItemArray[10],
                        SerialNumber = (string)row.ItemArray[11],
                        ManufacturerID = (int)row.ItemArray[12],
                        Manufacturer = (string)row.ItemArray[13],
                        ModelID = (int)row.ItemArray[14],
                        Model = (string)row.ItemArray[15],
                        TypeID = (int)row.ItemArray[16],
                        Type = (string)row.ItemArray[17],
                        CaliberGaugeID = (int)row.ItemArray[18],
                        CaliberOrGauge = (string)row.ItemArray[19],
                        AcquisitionDate = (DateTime)row.ItemArray[20],
                        StoreID = int.Parse(row.ItemArray[21].ToString()),
                        StoreAbbreviation = (string)row.ItemArray[22],
                        StoreDescription = (string)row.ItemArray[23],
                        SupplierID = (int)row.ItemArray[24],
                        SupplierName = (string)row.ItemArray[25],
                        SupplierAddress1 = (row.IsNull(26)) ? null : (string)row.ItemArray[26],
                        SupplierAddress2 = (row.IsNull(27)) ? null : (string)row.ItemArray[27],
                        SupplierCity = (row.IsNull(28)) ? null : (string)row.ItemArray[28],
                        SupplierState = (row.IsNull(29)) ? null : (string)row.ItemArray[29],
                        SupplierZip = (row.IsNull(30)) ? null : (string)row.ItemArray[30],
                        SupplierCountry = (row.IsNull(31)) ? null : (string)row.ItemArray[31],
                        SupplierLicenseNumber = (row.IsNull(32)) ? null : (string)row.ItemArray[32],
                        DispositionID = (row.IsNull(33)) ? (int?)null : (int)row.ItemArray[33],
                        DispositionDate = (row.IsNull(34)) ? (DateTime?)null : (DateTime)row.ItemArray[34]
                    };

                    if (!row.IsNull(35))
                        returnRecord.ImportPurchaserFirstName((string)row.ItemArray[35]);
                    if (!row.IsNull(36))
                        returnRecord.ImportPurchaserMiddleName((string)row.ItemArray[36]);
                    if (!row.IsNull(37))
                        returnRecord.ImportPurchaserLastName((string)row.ItemArray[37]);
                    if (!row.IsNull(38))
                        returnRecord.ImportPurchaserAddress1((string)row.ItemArray[38]);
                    if (!row.IsNull(39))
                        returnRecord.ImportPurchaserAddress2((string)row.ItemArray[39]);
                    if (!row.IsNull(40))
                        returnRecord.ImportPurchaserCity((string)row.ItemArray[40]);
                    if (!row.IsNull(41))
                        returnRecord.ImportPurchaserState((string)row.ItemArray[41]);
                    if (!row.IsNull(42))
                        returnRecord.ImportPurchaserZip((string)row.ItemArray[42]);
                    if (!row.IsNull(43))
                        returnRecord.ImportPurchaserCountry((string)row.ItemArray[43]);
                    if (!row.IsNull(44))
                        returnRecord.ImportPrimaryIdentificationNumber((string)row.ItemArray[44]);
                    returnRecord.Form4473SerialNumber = (row.IsNull(45)) ? null : (string)row.ItemArray[45];

                    if (int.TryParse((string)row.ItemArray[46], out int tempBinderNumber))
                    {
                        returnRecord.BinderNumber = tempBinderNumber;
                    }
                    else
                    {
                        returnRecord.BinderNumber = 0;
                    }
                    if (int.TryParse((string)row.ItemArray[47], out int tempLineItemNumber))
                    {
                        returnRecord.LineItemNumber = tempLineItemNumber;
                    }
                    else
                    {
                        returnRecord.LineItemNumber = 0;
                    }
                    returnRecord.PrimaryIdentifierType = (row.IsNull(48)) ? null : (string)row.ItemArray[48];
                }
                record = returnRecord;
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                record = new Record();
                return false;
            }
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
