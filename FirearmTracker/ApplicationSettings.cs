﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Utilities;

namespace FirearmTracker
{
    [XmlRoot(ElementName = "ApplicationSettings")]
    public class ApplicationSettings
    {
        private const string APPLICATION_SETTINGS_FILE = "aset.xml";

        private bool useCustomAcquisitonBeginningDate;
        private DateTime customAcquisitionBeginningDate;
        private string filePath;

        [XmlElement(ElementName = "UseCustomAcquisitonBeginningDate")]
        public bool UseCustomAcquisitonBeginningDate { get => useCustomAcquisitonBeginningDate; set => useCustomAcquisitonBeginningDate = value; }
        [XmlElement(ElementName = "CustomAcquisitionBeginningDate")]
        public DateTime CustomAcquisitionBeginningDate { get => customAcquisitionBeginningDate; set => customAcquisitionBeginningDate = value; }        

        public ApplicationSettings()
        {
            useCustomAcquisitonBeginningDate = false;
            customAcquisitionBeginningDate = DateTime.Now.AddMonths(-1);
            filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), APPLICATION_SETTINGS_FILE);
        }

        public void UseDefaultSettings()
        {
            useCustomAcquisitonBeginningDate = false;
            customAcquisitionBeginningDate = DateTime.Now.AddMonths(-1);
            filePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), APPLICATION_SETTINGS_FILE);
        }

        public void LoadFromFile()
        {
            var serializer = new XmlSerializer(typeof(ApplicationSettings));
            ApplicationSettings temp;
            try
            {
                using (var stream = File.OpenRead(filePath))
                {
                    temp = (ApplicationSettings)serializer.Deserialize(stream);
                    useCustomAcquisitonBeginningDate = temp.useCustomAcquisitonBeginningDate;
                    customAcquisitionBeginningDate = temp.customAcquisitionBeginningDate;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError($"Could not load {APPLICATION_SETTINGS_FILE}.  Using default settings instead.  {ex.Message}");
                UseDefaultSettings();
                SaveToFile();
            }
        }

        public void SaveToFile()
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(ApplicationSettings));
                XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
                using (XmlWriter s = XmlWriter.Create(filePath, settings))
                    xs.Serialize(s, this);
            }
            catch (Exception ex)
            {
                Logger.LogError($"Could not save {APPLICATION_SETTINGS_FILE}.  {ex.Message}");
            }
        }

    }
}
