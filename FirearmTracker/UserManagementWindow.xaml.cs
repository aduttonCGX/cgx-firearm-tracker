﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;


namespace FirearmTracker
{
    public partial class UserManagementWindow : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        private List<User> users;
        public UserManagementWindow()
        {
            InitializeComponent();
            GetUsers();            
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            //AddToDatabase();
        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }

        private void GetUsers()
        {
            users = new List<User>();
            string query = $@"SELECT UserName FROM [CGESFirearm].[dbo].[users] ORDER BY UserName";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    return;
                }
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection);

                cmd.CommandTimeout = 600;

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    users.Add(new User((string)row.ItemArray[0]));
                }

                lvUsers.ItemsSource = users;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return;
            }

            // Grouping
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvUsers.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Status");
            view.GroupDescriptions.Add(groupDescription);
        }

        private void BtnAddUser_Click(object sender, RoutedEventArgs e)
        {
            AddEditUserWindow addEditUserWindow = new AddEditUserWindow();
            addEditUserWindow.ShowDialog();

            if (addEditUserWindow.Result == WindowResult.OK)
            {
                GetUsers();
                CollectionViewSource.GetDefaultView(lvUsers.ItemsSource).Refresh();
            }
        }

        private void BtnEditUser_Click(object sender, RoutedEventArgs e)
        {
            if (lvUsers.SelectedIndex == -1)
            {
                return;
            }

            AddEditUserWindow addEditUserWindow = new AddEditUserWindow(lvUsers.SelectedItem as User);
            addEditUserWindow.ShowDialog();

            if (addEditUserWindow.Result == WindowResult.OK)
            {
                GetUsers();
                CollectionViewSource.GetDefaultView(lvUsers.ItemsSource).Refresh();
            }
        }

        void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = lvUsers.SelectedItem as User;
            if (item != null)
            {
                AddEditUserWindow addEditUserWindow = new AddEditUserWindow(item);
                addEditUserWindow.ShowDialog();

                if (addEditUserWindow.Result == WindowResult.OK)
                {
                    GetUsers();
                    CollectionViewSource.GetDefaultView(lvUsers.ItemsSource).Refresh();
                }
            }
        }
    }
}
