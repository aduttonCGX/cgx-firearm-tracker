﻿#pragma checksum "..\..\AddNewAcquisitionWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "BC0C6D4043CE41C59B66F3D06B26183E0FC92BB6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirearmTracker;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FirearmTracker {
    
    
    /// <summary>
    /// AddNewAcquisitionWindow
    /// </summary>
    public partial class AddNewAcquisitionWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxStore;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpAcquisitionDate;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSerialNumber;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxManufacturer;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewManufacturer;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxModel;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewModel;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxType;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewType;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxCaliber;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewCaliber;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxSupplier;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewSupplier;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOK;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\AddNewAcquisitionWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FirearmTracker;component/addnewacquisitionwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AddNewAcquisitionWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.cboxStore = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.dpAcquisitionDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 3:
            this.txtSerialNumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.cboxManufacturer = ((System.Windows.Controls.ComboBox)(target));
            
            #line 51 "..\..\AddNewAcquisitionWindow.xaml"
            this.cboxManufacturer.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboxManufacturer_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnNewManufacturer = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewManufacturer.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 56 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewManufacturer.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 56 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewManufacturer.Click += new System.Windows.RoutedEventHandler(this.BtnNewManufacturer_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.cboxModel = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.btnNewModel = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewModel.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 74 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewModel.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 74 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewModel.Click += new System.Windows.RoutedEventHandler(this.BtnNewModel_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.cboxType = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.btnNewType = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewType.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 85 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewType.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 85 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewType.Click += new System.Windows.RoutedEventHandler(this.BtnNewType_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.cboxCaliber = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.btnNewCaliber = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewCaliber.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 96 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewCaliber.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 96 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewCaliber.Click += new System.Windows.RoutedEventHandler(this.BtnNewCaliber_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.cboxSupplier = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 13:
            this.btnNewSupplier = ((System.Windows.Controls.Button)(target));
            
            #line 112 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewSupplier.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 112 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewSupplier.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 112 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnNewSupplier.Click += new System.Windows.RoutedEventHandler(this.BtnNewSupplier_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btnOK = ((System.Windows.Controls.Button)(target));
            
            #line 120 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnOK.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 120 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnOK.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 120 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnOK.Click += new System.Windows.RoutedEventHandler(this.BtnOK_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 132 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnCancel.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 132 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnCancel.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 132 "..\..\AddNewAcquisitionWindow.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.BtnCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

