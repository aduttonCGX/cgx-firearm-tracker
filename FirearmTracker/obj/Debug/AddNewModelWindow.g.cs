﻿#pragma checksum "..\..\AddNewModelWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4674814CDAD9ECF2C9054D91E203BE771ACCBC9A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirearmTracker;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FirearmTracker {
    
    
    /// <summary>
    /// AddNewModelWindow
    /// </summary>
    public partial class AddNewModelWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblTitle;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblManufacturer;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cboxManufacturer;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewManufacturer;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblInput;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblInputValidationText;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtInput;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOK;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\AddNewModelWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FirearmTracker;component/addnewmodelwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AddNewModelWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\AddNewModelWindow.xaml"
            ((FirearmTracker.AddNewModelWindow)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.lblTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.lblManufacturer = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.cboxManufacturer = ((System.Windows.Controls.ComboBox)(target));
            
            #line 18 "..\..\AddNewModelWindow.xaml"
            this.cboxManufacturer.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboxManufacturer_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnNewManufacturer = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\AddNewModelWindow.xaml"
            this.btnNewManufacturer.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 23 "..\..\AddNewModelWindow.xaml"
            this.btnNewManufacturer.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 23 "..\..\AddNewModelWindow.xaml"
            this.btnNewManufacturer.Click += new System.Windows.RoutedEventHandler(this.BtnNewManufacturer_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lblInput = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.lblInputValidationText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.txtInput = ((System.Windows.Controls.TextBox)(target));
            
            #line 35 "..\..\AddNewModelWindow.xaml"
            this.txtInput.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TxtInput_TextChanged);
            
            #line default
            #line hidden
            
            #line 35 "..\..\AddNewModelWindow.xaml"
            this.txtInput.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxtInput_KeyDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnOK = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\AddNewModelWindow.xaml"
            this.btnOK.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 47 "..\..\AddNewModelWindow.xaml"
            this.btnOK.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 47 "..\..\AddNewModelWindow.xaml"
            this.btnOK.Click += new System.Windows.RoutedEventHandler(this.BtnOK_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\AddNewModelWindow.xaml"
            this.btnCancel.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseDown);
            
            #line default
            #line hidden
            
            #line 59 "..\..\AddNewModelWindow.xaml"
            this.btnCancel.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(this.btn_MouseUp);
            
            #line default
            #line hidden
            
            #line 59 "..\..\AddNewModelWindow.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.BtnCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

