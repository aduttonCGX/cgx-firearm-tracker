﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddEditDispositionWindow.xaml
    /// </summary>
    public partial class AddEditDispositionWindow : Window
    {
        private bool buttonDepressed = false;
        private bool editMode = false;
        private Record record;
        private int acquisitionID;
        public WindowResult Result;

        // Open in "Add Disposition" mode
        public AddEditDispositionWindow(int acquisitionID)
        {
            InitializeComponent();
            editMode = false;
            this.acquisitionID = acquisitionID;
            dpDispositionDate.SelectedDate = DateTime.Today;
            txtFirstName.Focus();
            Title = lblTitle.Text;            
        }

        // Open in "Edit Disposition" mode
        public AddEditDispositionWindow(Record record)
        {
            InitializeComponent();
            editMode = true;
            this.record = record;
            lblTitle.Text = $"Edit Disposition #{this.record.DispositionID}";
            Title = lblTitle.Text;
            PopulateFields();
            txtFirstName.Focus();
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtPrimaryIdentificationNumber.Text))
            {
                // Check to see if there have been recent purchases under this License number
                List<Purchase> purchases = new List<Purchase>();
                try
                {
                    SqlDataReader reader = null;
                    using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("sp_GetRecentDispositions", conn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.Add(new SqlParameter("@LicenseNumber", Encrypt.EncryptString(txtPrimaryIdentificationNumber.Text.Trim(), "23ejhe8hoshfwoejnf9")));
                        cmd.Parameters.Add(new SqlParameter("@DispositionDate", dpDispositionDate.SelectedDate));
                        if (editMode)
                            cmd.Parameters.Add(new SqlParameter("@CurrentDispositionNumber", record.DispositionID));

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Purchase purchase = new Purchase
                            {
                                AcquisitionID = int.Parse(reader["AcquisitionID"].ToString()),
                                DispositionDate = DateTime.Parse(reader["DispositionDate"].ToString()),
                                Form4473SerialNumber = reader["Form4473SerialNumber"].ToString(),
                                WeaponDetails = reader["WeaponDetails"].ToString(),
                                Store = reader["Store"].ToString()
                            };
                            purchases.Add(purchase);
                        }
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                    return;
                }

                if (purchases.Count > 0)
                {
                    StringBuilder message = new StringBuilder();
                    message.AppendLine($"Primary Identification Number {txtPrimaryIdentificationNumber.Text} has been used in one or more recent transactions.  Please review the details below:");
                    foreach (Purchase purchase in purchases)
                    {
                        message.AppendLine(purchase.ToString());
                    }
                    message.AppendLine($"{Environment.NewLine}Do you wish to continue?");

                    if (MBWindow.Show(message.ToString(), "Warning", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    {
                        return;
                    }
                }
            }

            if (editMode)
            {
                // Update record with field data
                record.DispositionDate = dpDispositionDate.SelectedDate;
                record.PurchaserFirstName = txtFirstName.Text;
                record.PurchaserMiddleName = txtMiddleName.Text;
                record.PurchaserLastName = txtLastName.Text;
                record.PurchaserAddress1 = txtAddress1.Text;
                record.PurchaserAddress2 = txtAddress2.Text;
                record.PurchaserCity = txtCity.Text;
                record.PurchaserState = txtState.Text;
                record.PurchaserZip = txtZipCode.Text;
                record.PurchaserCountry = txtCountry.Text;
                record.PrimaryIdentifierType = txtPrimaryIdentificationType.Text;
                record.PrimaryIdentificationNumber = txtPrimaryIdentificationNumber.Text;
                record.Form4473SerialNumber = txtForm4473SerialNumber.Text;

                // Send update to Database
                UpdateDatabase();
            }
            else
            {                
                AddToDatabase();
            }            
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            this.Close();
        }

        private void PopulateFields()
        {
            dpDispositionDate.SelectedDate = record.DispositionDate.Value;
            txtFirstName.Text = record.PurchaserFirstName;
            txtMiddleName.Text = record.PurchaserMiddleName;
            txtLastName.Text = record.PurchaserLastName;
            txtAddress1.Text = record.PurchaserAddress1;
            txtAddress2.Text = record.PurchaserAddress2;
            txtCity.Text = record.PurchaserCity;
            txtState.Text = record.PurchaserState;
            txtZipCode.Text = record.PurchaserZip;
            txtCountry.Text = record.PurchaserCountry;
            txtPrimaryIdentificationType.Text = record.PrimaryIdentifierType;
            txtPrimaryIdentificationNumber.Text = record.PrimaryIdentificationNumber;
            txtForm4473SerialNumber.Text = record.Form4473SerialNumber;
        }

        private void UpdateDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtFirstName.Text) || string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                MBWindow.Show("Must include the purchaser's first and last name.", "Error", MessageBoxButton.OK);
                return;
            }

            if (string.IsNullOrWhiteSpace(txtForm4473SerialNumber.Text))
            {
                MBWindow.Show("Must include a Form 4473 serial number.", "Error", MessageBoxButton.OK);
                return;
            }

            // Update database
            
            
            try
            {
                record.UpdateRecordInDatabase();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            finally
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item updated successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
        }
        private void AddToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtFirstName.Text) || string.IsNullOrWhiteSpace(txtLastName.Text))
            {
                MBWindow.Show("Must include the purchaser's first and last name.", "Error", MessageBoxButton.OK);
                return;
            }

            if ((string.IsNullOrWhiteSpace(txtAddress1.Text) || string.IsNullOrWhiteSpace(txtCity.Text) || string.IsNullOrWhiteSpace(txtState.Text) || string.IsNullOrWhiteSpace(txtZipCode.Text)) && string.IsNullOrWhiteSpace(txtPrimaryIdentificationNumber.Text) && string.IsNullOrWhiteSpace(txtForm4473SerialNumber.Text))
            {
                MBWindow.Show("Must include a complete purchaser address, license number, or Form 4473 serial number.", "Error", MessageBoxButton.OK);
                return;
            }

            // Add to database
            record = new Record
            {
                AcquisitionID = acquisitionID,
                DispositionDate = dpDispositionDate.SelectedDate,
                PurchaserFirstName = txtFirstName.Text,
                PurchaserMiddleName = txtMiddleName.Text,
                PurchaserLastName = txtLastName.Text,
                PurchaserAddress1 = txtAddress1.Text,
                PurchaserAddress2 = txtAddress2.Text,
                PurchaserCity = txtCity.Text,
                PurchaserState = txtState.Text,
                PurchaserZip = txtZipCode.Text,
                PurchaserCountry = txtCountry.Text,
                PrimaryIdentifierType = txtPrimaryIdentificationType.Text,
                PrimaryIdentificationNumber = txtPrimaryIdentificationNumber.Text,
                Form4473SerialNumber = txtForm4473SerialNumber.Text
            };

            if (record.AddDispositionToDatabase())
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
            else
            {
                MBWindow.Show($"Item failed to add.  Please check the error log at {Logger.ErrorLogFile}.", "Error", MessageBoxButton.OK);
            }            
        }
    }
}
