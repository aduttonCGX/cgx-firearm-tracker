﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Firearm;
using Utilities;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for ReportConsole.xaml
    /// </summary>
    public enum ReportType { OpenAcquisition, Disposition }
    public partial class ReportConsole : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        private User user;
        private Store store;
        private ReportType reportType;
        private OrderByColumn column;

        public ReportConsole(ReportType reportType, User user, Store store)
        {
            InitializeComponent();
            this.reportType = reportType;
            this.user = user;
            this.store = store;
            this.column = OrderByColumn.AcquisitionID;
            switch (reportType)
            {
                case ReportType.OpenAcquisition:
                    pnlDispositionReport.Visibility = Visibility.Collapsed;
                    pnlOpenAcquisitionReport.Visibility = Visibility.Visible;
                    dpOpenAcquisitionEndDate.SelectedDate = DateTime.Today;
                    dpOpenAcquisitionStartDate.SelectedDate = DateTime.Today.AddMonths(-1);
                    lblOpenAcquisitionsStore.Text = $"{store.StoreDescription}";
                    Height = 300;
                    break;
                case ReportType.Disposition:
                    pnlDispositionReport.Visibility = Visibility.Visible;
                    pnlOpenAcquisitionReport.Visibility = Visibility.Collapsed;
                    dpDispositionEndDate.SelectedDate = DateTime.Today;
                    dpDispositionStartDate.SelectedDate = DateTime.Today.AddMonths(-1);
                    lblDispositionStore.Text = $"{store.StoreDescription}";
                    Height = 260;
                    break;
            }
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void BtnDispositionGenerate_Click(object sender, RoutedEventArgs e)
        {
            DispositionReport dispositionReport = new DispositionReport(dpDispositionStartDate.SelectedDate.Value, dpDispositionEndDate.SelectedDate.Value, store, column);
            dispositionReport.QueryData();
            dispositionReport.BuildReport();
            Result = WindowResult.OK;
            Close();
        }

        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }

        private void BtnOpenAcquisitionsGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (rbOpenAquisitionAll.IsChecked.Value)
            {                
                try
                {
                    OpenAcquisitionReport openAcquisitionReport = new OpenAcquisitionReport(user, store, column);
                    openAcquisitionReport.QueryData();
                    openAcquisitionReport.BuildReport();
                    Result = WindowResult.OK;
                    Close();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    MBWindow.Show(ex.Message, "Error", MessageBoxButton.OK);
                    Close();
                }
            }
            else
            {                
                try
                {
                    OpenAcquisitionReport openAcquisitionReport = new OpenAcquisitionReport(dpOpenAcquisitionStartDate.SelectedDate.Value, dpOpenAcquisitionEndDate.SelectedDate.Value, user, store, column);
                    openAcquisitionReport.QueryData();
                    openAcquisitionReport.BuildReport();
                    Result = WindowResult.OK;
                    Close();

                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    MBWindow.Show(ex.Message, "Error", MessageBoxButton.OK);
                    Close();
                }
            }
            
        }

        private void RbOpenAquisitionRange_Click(object sender, RoutedEventArgs e)
        {
            dpOpenAcquisitionEndDate.IsEnabled = true;
            dpOpenAcquisitionStartDate.IsEnabled = true;
        }

        private void RbOpenAquisitionAll_Click(object sender, RoutedEventArgs e)
        {
            dpOpenAcquisitionEndDate.IsEnabled = false;
            dpOpenAcquisitionStartDate.IsEnabled = false;
        }
                
        private void RbAcquisitionSortByAcquisitionID_Click(object sender, RoutedEventArgs e)
        {
            column = OrderByColumn.AcquisitionID;
        }

        private void RbAcquisitionSortByBinder_Click(object sender, RoutedEventArgs e)
        {
            column = OrderByColumn.BinderNumber;
        }

        private void RbDispositionSortByAcquisitionID_Click(object sender, RoutedEventArgs e)
        {
            column = OrderByColumn.AcquisitionID;
        }

        private void RbDispositionSortByBinder_Click(object sender, RoutedEventArgs e)
        {
            column = OrderByColumn.BinderNumber;
        }
    }
}
