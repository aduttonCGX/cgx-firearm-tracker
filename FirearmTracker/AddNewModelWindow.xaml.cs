﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddNewModelWindow.xaml
    /// </summary>
    public partial class AddNewModelWindow : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        private List<string> existingModels;
        private List<Manufacturer> manufacturers;

        public AddNewModelWindow()
        {
            InitializeComponent();
            GetManufacturers();
            Title = lblTitle.Text;
            lblInputValidationText.Text = "";
            
            // Keep the input field disabled until the user selects a manufacturer
            txtInput.IsEnabled = false;
        }

        public AddNewModelWindow(int selectedIndex)
        {
            InitializeComponent();
            GetManufacturers();
            Title = lblTitle.Text;
            lblInputValidationText.Text = "";

            // Keep the input field disabled until the user selects a manufacturer
            txtInput.IsEnabled = false;
            cboxManufacturer.SelectedIndex = selectedIndex;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            AddItemToDatabase();
        }
        private void AddItemToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text) || cboxManufacturer.SelectedIndex == -1)
            {
                MBWindow.Show("All fields are required.", "Error", MessageBoxButton.OK);
                return;
            }

            if (lblInputValidationText.Text != "")
            {
                MBWindow.Show("Please correct validation errors.", "Error", MessageBoxButton.OK);
                return;
            }

            // Add to database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    SqlCommand manufacturerCmd = new SqlCommand("sp_AddModel", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    manufacturerCmd.Parameters.Add(new SqlParameter("@ManufacturerID", cboxManufacturer.SelectedValue));
                    manufacturerCmd.Parameters.Add(new SqlParameter("@ModelDescription", txtInput.Text));
                    manufacturerCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            finally
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }
        private void TxtInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text))
            {
                lblInputValidationText.Text = "";
                return;
            }

            if (existingModels.FirstOrDefault(element => element.Equals(txtInput.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) != null)
            {
                lblInputValidationText.Text = "Item already exists.";
            }
            else
            {
                lblInputValidationText.Text = "";
            }
        }
        private void GetExistingModels(string manufacturerID)
        {
            existingModels = new List<string>();
            string query = $"SELECT ModelDescription FROM [CGESFirearm].[dbo].[models] WHERE ManufacturerID = '{manufacturerID}'";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    existingModels.Add((string)row.ItemArray[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void GetManufacturers()
        {
            manufacturers = new List<Manufacturer>();
            string query = @"SELECT ManufacturerID, ManufacturerDescription FROM [CGESFirearm].[dbo].[manufacturers] ORDER BY ManufacturerDescription";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    manufacturers.Add(new Manufacturer((int)row.ItemArray[0], (string)row.ItemArray[1]));
                }

                // Bind to the combobox
                cboxManufacturer.ItemsSource = manufacturers;
                cboxManufacturer.DisplayMemberPath = "ManufacturerDescription";
                cboxManufacturer.SelectedValuePath = "ManufacturerID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }

        private void TxtInput_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case (Key.Enter):
                    AddItemToDatabase();
                    break;
                case (Key.Escape):
                    Result = WindowResult.Cancel;
                    Close();
                    break;
                default:
                    break;
            }
        }

        private void CboxManufacturer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cboxManufacturer.SelectedIndex == -1)
            {
                txtInput.IsEnabled = false;
                return;
            }

            txtInput.IsEnabled = true;
            GetExistingModels(cboxManufacturer.SelectedValue.ToString());
            txtInput.Focus();
        }

        private void BtnNewManufacturer_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.Manufacturer);
            addNewDetailWindow.ShowDialog();

            if (addNewDetailWindow.Result == WindowResult.OK)
            {
                GetManufacturers();
            }
        }
    }    
}
