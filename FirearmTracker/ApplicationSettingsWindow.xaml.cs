﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for ApplicationSettingsWindow.xaml
    /// </summary>
    public partial class ApplicationSettingsWindow : Window
    {
        private bool buttonDepressed;
        private ApplicationSettings applicationSettings;
        public WindowResult Result;
        public ApplicationSettingsWindow()
        {
            InitializeComponent();
            applicationSettings = new ApplicationSettings();
            LoadSettings();
            Result = WindowResult.Cancel;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (cbUseCustomAcquisitonBeginningDate.IsChecked.Value && !dpAcquisitionStartingDate.SelectedDate.HasValue )
            {
                MBWindow.Show("You must select a date if the custom beginning date option is checked.", "Error", MessageBoxButton.OK);
                return;
            }

            SaveSettings();
            Result = WindowResult.OK;
            Close();
        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LoadSettings()
        {
            applicationSettings.LoadFromFile();
            cbUseCustomAcquisitonBeginningDate.IsChecked = applicationSettings.UseCustomAcquisitonBeginningDate;
            if (cbUseCustomAcquisitonBeginningDate.IsChecked.Value)
            {
                dpAcquisitionStartingDate.SelectedDate = applicationSettings.CustomAcquisitionBeginningDate;
            }
        }

        private void SaveSettings()
        {
            applicationSettings.UseCustomAcquisitonBeginningDate = cbUseCustomAcquisitonBeginningDate.IsChecked.Value;
            if (cbUseCustomAcquisitonBeginningDate.IsChecked.Value)
                applicationSettings.CustomAcquisitionBeginningDate = dpAcquisitionStartingDate.SelectedDate.Value;
            applicationSettings.SaveToFile();
        }
    }
}
