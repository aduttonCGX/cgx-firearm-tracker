﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GridViewColumnHeader listViewSortCol = null;
        private SortAdorner listViewSortAdorner = null;
        private User currentUser;
        private bool buttonDepressed = false;
        private ApplicationSettings applicationSettings;
        private List<Record> records;

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                currentUser = new User(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                lblUsername.Text = $"User: {currentUser.LongName}";
                lblMachine.Text = $"Machine Name: {Environment.MachineName}";
                applicationSettings = new ApplicationSettings();
                applicationSettings.LoadFromFile();

                if (currentUser.IsAdmin)
                {
                    menuItemNewStore.Visibility = Visibility.Visible;
                    mhdAdministrator.Visibility = Visibility.Visible;
                    this.Title += " [Administrator]";
                }
                else
                {
                    mhdAdministrator.Visibility = Visibility.Collapsed;
                    menuItemNewStore.Visibility = Visibility.Collapsed;
                }

                // Hide all buttons
                btnAddDisposition.Visibility = Visibility.Hidden;
                btnDeleteAcquisiton.Visibility = Visibility.Hidden;
                btnDeleteDisposition.Visibility = Visibility.Hidden;
                btnEditAcquisiton.Visibility = Visibility.Hidden;
                btnEditDisposition.Visibility = Visibility.Hidden;
                lblDateDeleted.Visibility = Visibility.Hidden;

                // Hide Disposition Labels
                lblDispositionDetailsLabel.Visibility = Visibility.Hidden;
                lblDispositionCodeLabel.Visibility = Visibility.Hidden;
                lblDispositionDateLabel.Visibility = Visibility.Hidden;
                lblPurchaserLabel.Visibility = Visibility.Hidden;
                lblDispositionCreatedLabel.Visibility = Visibility.Hidden;
                lblDispositionUpdatedLabel.Visibility = Visibility.Hidden;

                records = new List<Record>();
                lvAcquisitions.ItemsSource = records;

                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource);
                PropertyGroupDescription groupDescription = new PropertyGroupDescription("Status");
                view.GroupDescriptions.Add(groupDescription);

                view.SortDescriptions.Add(new SortDescription("Status", ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription("AcquisitionID", ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription("SerialNumber", ListSortDirection.Ascending));
                view.Filter = UserFilter;

                // Set date range 
                if (applicationSettings.UseCustomAcquisitonBeginningDate)
                    dtpAcquisitionFromDateFilter.SelectedDate = applicationSettings.CustomAcquisitionBeginningDate;
                else
                    dtpAcquisitionFromDateFilter.SelectedDate = DateTime.Now.AddMonths(-1);
                dtpAcquisitionToDateFilter.SelectedDate = DateTime.Now;

                // Populate the store selection menu
                GetStores();
                cboxSelectedStore.SelectedIndex = 0;

                RoutedCommand newCmd = new RoutedCommand();
                newCmd.InputGestures.Add(new KeyGesture(Key.F2));
                CommandBindings.Add(new CommandBinding(newCmd, MenuItemNewAcquisition_Click));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }            
        }

        private void lvAcquisitionsColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = (sender as GridViewColumnHeader);
            string sortBy = column.Tag.ToString();
            if (listViewSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(listViewSortCol).Remove(listViewSortAdorner);
                lvAcquisitions.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (listViewSortCol == column && listViewSortAdorner.Direction == newDir)
                newDir = ListSortDirection.Descending;

            listViewSortCol = column;
            listViewSortAdorner = new SortAdorner(listViewSortCol, newDir);
            AdornerLayer.GetAdornerLayer(listViewSortCol).Add(listViewSortAdorner);
            lvAcquisitions.Items.SortDescriptions.Add(new SortDescription("Status", ListSortDirection.Ascending));
            lvAcquisitions.Items.SortDescriptions.Add(new SortDescription(sortBy, newDir));
        }

        #region Filters
        private bool UserFilter(object item)
        {
            return IDFilter(item) &&
                   DescriptionFilter(item) &&
                   SerialNumberFilter(item) &&
                   AcquisitionDateFilter(item) &&
                   DispositionDateFilter(item) &&
                   DeletedItemFilter(item) &&
                   PurchaserNameFilter(item) &&
                   Form4473SerialNumberFilter(item) &&
                   SupplierFilter(item) &&
                   BinderNumberFilter(item) &&
                   LineItemNumberFilter(item);
        }
        private bool PurchaserNameFilter(object item)
        {
            if (String.IsNullOrEmpty(txtPurchaserNameFilter.Text))
                return true;
            else
            {
                if (String.IsNullOrEmpty((item as Record).PurchaserName))
                    return false;
                else
                    return ((item as Record).PurchaserName.ToString().IndexOf(txtPurchaserNameFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            }                
        }
        private bool Form4473SerialNumberFilter(object item)
        {
            if (String.IsNullOrEmpty(txtForm4473SerialNumberFilter.Text))
                return true;

            if (String.IsNullOrEmpty((item as Record).Form4473SerialNumber))
                return false;
            else
                return ((item as Record).Form4473SerialNumber.ToString().IndexOf(txtForm4473SerialNumberFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
        private bool DeletedItemFilter(object item)
        {
            return ((item as Record).Status != StatusType.Deleted || currentUser.IsAdmin);
        }
        private bool AcquisitionDateFilter(object item)
        {
            // No date boundary defined
            if (dtpAcquisitionToDateFilter.SelectedDate == null && dtpAcquisitionFromDateFilter.SelectedDate == null)
                return true;

            // Max date undefined
            if (dtpAcquisitionToDateFilter.SelectedDate == null)
                return (item as Record).AcquisitionDate.Date >= dtpAcquisitionFromDateFilter.SelectedDate.Value.Date;                

            // Min date undefined
            if (dtpAcquisitionFromDateFilter.SelectedDate == null)
                return (item as Record).AcquisitionDate.Date <= dtpAcquisitionToDateFilter.SelectedDate.Value.Date;
            
            // Both dates defined
            return ((item as Record).AcquisitionDate.Date >= dtpAcquisitionFromDateFilter.SelectedDate.Value.Date
                && (item as Record).AcquisitionDate.Date <= dtpAcquisitionToDateFilter.SelectedDate.Value.Date);
        }
        private bool DispositionDateFilter(object item)
        {
            try
            {
                // No date boundary defined
                if (dtpDispositionToDateFilter.SelectedDate == null && dtpDispositionFromDateFilter.SelectedDate == null)
                    return true;

                // Max date undefined
                if (dtpDispositionToDateFilter.SelectedDate == null)
                    return (item as Record).DispositionDate.Value.Date >= dtpDispositionFromDateFilter.SelectedDate.Value.Date;

                // Min date undefined
                if (dtpDispositionFromDateFilter.SelectedDate == null)
                    return (item as Record).DispositionDate.Value.Date <= dtpDispositionToDateFilter.SelectedDate.Value.Date;

                // Both dates defined
                return ((item as Record).DispositionDate.Value.Date >= dtpDispositionFromDateFilter.SelectedDate.Value.Date
                    && (item as Record).DispositionDate.Value.Date <= dtpDispositionToDateFilter.SelectedDate.Value.Date);
            }
            catch (System.InvalidOperationException)
            {
                // No Disposition
                return false;
            }
        }
        private bool IDFilter(object item)
        {
            if (String.IsNullOrEmpty(txtIDFilter.Text))
                return true;
            else
                return ((item as Record).AcquisitionID.ToString().IndexOf(txtIDFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
        private bool DescriptionFilter(object item)
        {
            if (String.IsNullOrEmpty(txtDescriptionFilter.Text))
                return true;
            else
                return ((item as Record).Description.IndexOf(txtDescriptionFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
        private bool SupplierFilter(object item)
        {
            if (String.IsNullOrEmpty(txtSupplierFilter.Text))
                return true;
            else
                return ((item as Record).SupplierName.IndexOf(txtSupplierFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
        private bool SerialNumberFilter(object item)
        {
            if (String.IsNullOrEmpty(txtSerialNumberFilter.Text))
                return true;
            else
                return ((item as Record).SerialNumber.IndexOf(txtSerialNumberFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
        private bool BinderNumberFilter(object item)
        {
            if (String.IsNullOrEmpty(txtBinderFilter.Text))
                return true;
            else
            {
                if (string.IsNullOrEmpty((item as Record).BinderNumber.ToString()))
                    return false;
                else
                    return ((item as Record).BinderNumber.ToString().IndexOf(txtBinderFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            }
                
        }
        private bool LineItemNumberFilter(object item)
        {
            if (String.IsNullOrEmpty(txtLineItemFilter.Text))
                return true;
            else
            {
                if (string.IsNullOrEmpty((item as Record).LineItemNumber.ToString()))
                    return false;
                else
                    return ((item as Record).LineItemNumber.ToString().IndexOf(txtLineItemFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            }
                
        }
        private void txtFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource).Refresh();
        }
        private void dtpFilter_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource).Refresh();
        }
        #endregion

        private void lvAcquisitions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvAcquisitions.SelectedItem == null)
                return;

            // Acquisition Data
            Record selectedRecord = lvAcquisitions.SelectedItem as Record;
            lblAcquisitionCode.Text = selectedRecord.AcquisitionID.ToString();
            lblBinderLocation.Text = $"{selectedRecord.BinderNumber}-{selectedRecord.LineItemNumber}";
            lblAcquisitionDate.Text = selectedRecord.AcquisitionDate.ToString("d");
            lblSerialNumber.Text = selectedRecord.SerialNumber;

            lblSupplierName.Text = selectedRecord.SupplierName;
            List<string> supplierData = new List<string>();

            if (selectedRecord.SupplierAddress1 != null)
            {
                supplierData.Add(selectedRecord.SupplierAddress1);
                if (selectedRecord.SupplierAddress2 != null)
                {
                    supplierData.Add(selectedRecord.SupplierAddress2);
                }
                supplierData.Add($"{selectedRecord.SupplierCity}, {selectedRecord.SupplierState} {selectedRecord.SupplierZip} {selectedRecord.SupplierCountry}");
            }
            if (selectedRecord.SupplierLicenseNumber != null)
            {
                supplierData.Add($"License #: {selectedRecord.SupplierLicenseNumber}");
            }

            lblSupplierDetails1.Text = "";
            lblSupplierDetails2.Text = "";
            lblSupplierDetails3.Text = "";
            lblSupplierDetails4.Text = "";

            if (supplierData.Count > 0)
            {
                lblSupplierDetails1.Text = supplierData[0];
            }            
            if (supplierData.Count > 1)
            {
                lblSupplierDetails2.Text = supplierData[1];
            }
            if (supplierData.Count > 2)
            {
                lblSupplierDetails3.Text = supplierData[2];
            }
            if (supplierData.Count > 3)
            {
                lblSupplierDetails4.Text = supplierData[3];
            }

            lblManufacturer.Text = selectedRecord.Manufacturer;
            lblModel.Text = selectedRecord.Model;
            lblDateCreated.Text = $"{selectedRecord.DateAcquisitionCreated.ToString("M/d/yyyy HH:mm:ss")} by {selectedRecord.UserAcquisitionCreated}";
            lblType.Text = selectedRecord.Type;
            lblCaliber.Text = selectedRecord.CaliberOrGauge;
            lblDateLastUpdated.Text = $"{selectedRecord.DateAcquisitionLastUpdated.ToString("M/d/yyyy HH:mm:ss")} by {selectedRecord.UserAcquisitionLastUpdated}";

            // Set buttons
            if ((selectedRecord.Status == StatusType.Open))
            {
                btnAddDisposition.Visibility = Visibility.Visible;
            }
            if (currentUser.IsAdmin)
            {
                btnEditAcquisiton.Visibility = Visibility.Visible;
                btnDeleteAcquisiton.Visibility = Visibility.Visible;
            }
            
            // If item is deleted
            if (selectedRecord.Status == StatusType.Deleted)
            {
                btnEditAcquisiton.Visibility = Visibility.Hidden;
                btnDeleteAcquisiton.Visibility = Visibility.Hidden;
                btnAddDisposition.Visibility = Visibility.Hidden;
                lblDateDeleted.Visibility = Visibility.Visible;
                lblDateDeleted.Text = $"Deleted {selectedRecord.DateDeleted.Value.ToString("M/d/yyyy HH:mm:ss")} by {selectedRecord.UserDeleted}";
            }
            else
            {
                lblDateDeleted.Visibility = Visibility.Hidden;
                lblDateDeleted.Text = "";
            }

            // Disposition Data
            if (selectedRecord.DispositionID != null)
            {
                // Set buttons
                btnAddDisposition.Visibility = Visibility.Hidden;
                if (currentUser.IsAdmin)
                {
                    btnEditDisposition.Visibility = Visibility.Visible;
                    btnDeleteDisposition.Visibility = Visibility.Visible;
                }
                lblDispositionDetailsLabel.Visibility = Visibility.Visible;
                lblDispositionCodeLabel.Visibility = Visibility.Visible;
                lblDispositionDateLabel.Visibility = Visibility.Visible;
                lblPurchaserLabel.Visibility = Visibility.Visible;
                lblDispositionCreatedLabel.Visibility = Visibility.Visible;
                lblDispositionUpdatedLabel.Visibility = Visibility.Visible;

                lblDispositionCode.Text = selectedRecord.DispositionID.ToString();
                lblDispositionDate.Text = selectedRecord.DispositionDate == null ? "" : selectedRecord.DispositionDate.Value.ToString("d");
                lblPurchaserName.Text = selectedRecord.PurchaserName;                
                
                lblPurchaserDetails1.Text = "";
                lblPurchaserDetails2.Text = "";
                lblPurchaserDetails3.Text = "";
                lblPurchaserDetails4.Text = "";
                lblPurchaserDetails5.Text = "";

                List<string> purchaserData = new List<string>();
                if (selectedRecord.PurchaserAddress1 != null)
                {
                    purchaserData.Add(selectedRecord.PurchaserAddress1);
                    if (selectedRecord.PurchaserAddress2 != null)
                    {
                        purchaserData.Add(selectedRecord.PurchaserAddress2);
                    }
                    purchaserData.Add($"{selectedRecord.PurchaserCity}, {selectedRecord.PurchaserState} {selectedRecord.PurchaserZip} {selectedRecord.PurchaserCountry}");
                }
                if (selectedRecord.PrimaryIdentificationNumber != null)
                {
                    purchaserData.Add($"{selectedRecord.PrimaryIdentifierType}: {selectedRecord.PrimaryIdentificationNumber}");                    
                }
                if (selectedRecord.Form4473SerialNumber != null)
                {
                    purchaserData.Add($"Form 4473 SN: {selectedRecord.Form4473SerialNumber}");                    
                }

                if (purchaserData.Count > 0)
                {
                    lblPurchaserDetails1.Text = purchaserData[0];
                }
                if (purchaserData.Count > 1)
                {
                    lblPurchaserDetails2.Text = purchaserData[1];
                }
                if (purchaserData.Count > 2)
                {
                    lblPurchaserDetails3.Text = purchaserData[2];
                }
                if (purchaserData.Count > 3)
                {
                    lblPurchaserDetails4.Text = purchaserData[3];
                }
                if (purchaserData.Count > 4)
                {
                    lblPurchaserDetails5.Text = purchaserData[4];
                }

                lblDispositionCreated.Text = $"{selectedRecord.DateDispositionCreated.Value.ToString("M/d/yyyy HH:mm:ss")} by {selectedRecord.UserDispositionCreated}";
                lblDispositionLastUpdated.Text = $"{selectedRecord.DateDispositionLastUpdated.Value.ToString("M/d/yyyy HH:mm:ss")} by {selectedRecord.UserDispositionLastUpdated}";                
            }
            else
            {
                lblDispositionCode.Text = "";
                lblDispositionDate.Text = "";
                lblPurchaserName.Text = "";
                lblPurchaserDetails1.Text = "";
                lblPurchaserDetails2.Text = "";
                lblPurchaserDetails3.Text = "";
                lblPurchaserDetails4.Text = "";
                lblPurchaserDetails5.Text = "";
                lblDispositionCreated.Text = "";
                lblDispositionLastUpdated.Text = "";
                btnEditDisposition.Visibility = Visibility.Hidden;
                btnDeleteDisposition.Visibility = Visibility.Hidden;

                lblDispositionDetailsLabel.Visibility = Visibility.Hidden;
                lblDispositionCodeLabel.Visibility = Visibility.Hidden;
                lblDispositionDateLabel.Visibility = Visibility.Hidden;
                lblPurchaserLabel.Visibility = Visibility.Hidden;
                lblDispositionCreatedLabel.Visibility = Visibility.Hidden;
                lblDispositionUpdatedLabel.Visibility = Visibility.Hidden;
            }
        }
        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newHeight = e.NewSize.Height - 550;
            if (newHeight < 200)
                lvAcquisitions.Height = 200;
            else
                lvAcquisitions.Height = newHeight;
        }
        private void BtnDeleteAcquisiton_Click(object sender, RoutedEventArgs e)
        {
            DeleteSelectedItem();
        }
        private void DeleteSelectedItem()
        {
            if (lvAcquisitions.SelectedIndex == -1)
                return;

            if ((lvAcquisitions.SelectedItem as Record).Status == StatusType.Deleted)
                return;

            if (!currentUser.IsAdmin)
                return;

            int selectionIndex = lvAcquisitions.SelectedIndex;

            if (MBWindow.Show($"Are you sure you want to delete Acquisition {(lvAcquisitions.SelectedItem as Record).AcquisitionID}?", "Confirm Delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                (lvAcquisitions.SelectedItem as Record).DeleteAcquisition();
                CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource).Refresh();
                lvAcquisitions.SelectedIndex = selectionIndex;
            }
        }
        private void lvAcquisitions_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    DeleteSelectedItem();
                    break;
                default:
                    break;
            }
        }
        private void CboxSelectedStore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetAcquisitions();
            CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource).Refresh();
            if (lvAcquisitions.Items.Count > 0)
                lvAcquisitions.SelectedIndex = 0;
        }

        #region Menu Item Commands
        private void MenuItemNewStore_Click(object sender, RoutedEventArgs e)
        {
            AddStoreWindow addStoreWindow = new AddStoreWindow();
            addStoreWindow.ShowDialog();

            if (addStoreWindow.Result == WindowResult.OK)
            {
                GetStores();
            }
        }       
        private void MenuItemNewSupplier_Click(object sender, RoutedEventArgs e)
        {
            AddSupplierWindow addSupplierWindow = new AddSupplierWindow();
            addSupplierWindow.ShowDialog();
        }
        private void MenuItemNewManifacturer_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.Manufacturer);
            addNewDetailWindow.ShowDialog();
        }
        private void MenuItemNewModel_Click(object sender, RoutedEventArgs e)
        {
            AddNewModelWindow addNewModelWindow = new AddNewModelWindow();
            addNewModelWindow.ShowDialog();
        }
        private void MenuItemNewType_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.Type);
            addNewDetailWindow.ShowDialog();
        }
        private void MenuItemNewCaliberGauge_Click(object sender, RoutedEventArgs e)
        {
            AddNewDetailWindow addNewDetailWindow = new AddNewDetailWindow(DetailType.CaliberGauge);
            addNewDetailWindow.ShowDialog();
        }
        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void MenuItemNewAcquisition_Click(object sender, RoutedEventArgs e)
        {
            string storeCode = (cboxSelectedStore.SelectedValue == null) ? (cboxSelectedStore.Items[0] as Store).StoreID : cboxSelectedStore.SelectedValue.ToString();
            AddEditAcquisitionWindow addEditAcquisitionWindow = new AddEditAcquisitionWindow(storeCode, currentUser.Stores);
            addEditAcquisitionWindow.ShowDialog();

            if (addEditAcquisitionWindow.Result == WindowResult.OK)
            {
                GetAcquisitions();
            }
        }
        #endregion
        private void GetAcquisitions()
        {
            if (cboxSelectedStore.SelectedValue == null)
            {
                return;
            }

            records = new List<Record>();
            string query = $@"SELECT * FROM [CGESFirearm].[dbo].[v_RecordsFullDetails] WHERE StoreID = {cboxSelectedStore.SelectedValue.ToString()}";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Record record = new Record
                    {
                        AcquisitionID = (int)row.ItemArray[0],
                        DateAcquisitionCreated = (DateTime)row.ItemArray[1],
                        UserAcquisitionCreated = (string)row.ItemArray[2],
                        DateDispositionCreated = (row.IsNull(3)) ? (DateTime?)null : (DateTime)row.ItemArray[3],
                        UserDispositionCreated = (row.IsNull(4)) ? null : (string)row.ItemArray[4],
                        DateDeleted = (row.IsNull(5)) ? (DateTime?)null : (DateTime)row.ItemArray[5],
                        UserDeleted = (row.IsNull(6)) ? null : (string)row.ItemArray[6],
                        DateAcquisitionLastUpdated = (DateTime)row.ItemArray[7],
                        UserAcquisitionLastUpdated = (string)row.ItemArray[8],
                        DateDispositionLastUpdated = (row.IsNull(9)) ? (DateTime?)null : (DateTime)row.ItemArray[9],
                        UserDispositionLastUpdated = (row.IsNull(10)) ? null : (string)row.ItemArray[10],
                        SerialNumber = (string)row.ItemArray[11],
                        ManufacturerID = (int)row.ItemArray[12],
                        Manufacturer = (string)row.ItemArray[13],
                        ModelID = (int)row.ItemArray[14],
                        Model = (string)row.ItemArray[15],
                        TypeID = (int)row.ItemArray[16],
                        Type = (string)row.ItemArray[17],
                        CaliberGaugeID = (int)row.ItemArray[18],
                        CaliberOrGauge = (string)row.ItemArray[19],
                        AcquisitionDate = (DateTime)row.ItemArray[20],
                        StoreID = int.Parse(row.ItemArray[21].ToString()),
                        StoreAbbreviation = (string)row.ItemArray[22],
                        StoreDescription = (string)row.ItemArray[23],
                        SupplierID = (int)row.ItemArray[24],
                        SupplierName = (string)row.ItemArray[25],
                        SupplierAddress1 = (row.IsNull(26)) ? null : (string)row.ItemArray[26],
                        SupplierAddress2 = (row.IsNull(27)) ? null : (string)row.ItemArray[27],
                        SupplierCity = (row.IsNull(28)) ? null : (string)row.ItemArray[28],
                        SupplierState = (row.IsNull(29)) ? null : (string)row.ItemArray[29],
                        SupplierZip = (row.IsNull(30)) ? null : (string)row.ItemArray[30],
                        SupplierCountry = (row.IsNull(31)) ? null : (string)row.ItemArray[31],
                        SupplierLicenseNumber = (row.IsNull(32)) ? null : (string)row.ItemArray[32],
                        DispositionID = (row.IsNull(33)) ? (int?)null : (int)row.ItemArray[33],
                        DispositionDate = (row.IsNull(34)) ? (DateTime?)null : (DateTime)row.ItemArray[34]
                    };

                    if (!row.IsNull(35))
                        record.ImportPurchaserFirstName((string)row.ItemArray[35]);
                    if (!row.IsNull(36))
                        record.ImportPurchaserMiddleName((string)row.ItemArray[36]);
                    if (!row.IsNull(37))
                        record.ImportPurchaserLastName((string)row.ItemArray[37]);
                    if (!row.IsNull(38))
                        record.ImportPurchaserAddress1((string)row.ItemArray[38]);
                    if (!row.IsNull(39))
                        record.ImportPurchaserAddress2((string)row.ItemArray[39]);
                    if (!row.IsNull(40))
                        record.ImportPurchaserCity((string)row.ItemArray[40]);
                    if (!row.IsNull(41))
                        record.ImportPurchaserState((string)row.ItemArray[41]);
                    if (!row.IsNull(42))
                        record.ImportPurchaserZip((string)row.ItemArray[42]);
                    if (!row.IsNull(43))
                        record.ImportPurchaserCountry((string)row.ItemArray[43]);
                    if (!row.IsNull(44))
                        record.ImportPrimaryIdentificationNumber((string)row.ItemArray[44]);
                    record.Form4473SerialNumber = (row.IsNull(45)) ? null : (string)row.ItemArray[45];

                    if (int.TryParse((string)row.ItemArray[46], out int tempBinderNumber))
                    {
                        record.BinderNumber = tempBinderNumber;
                    }
                    else
                    {
                        record.BinderNumber = 0;
                    }
                    if (int.TryParse((string)row.ItemArray[47], out int tempLineItemNumber))
                    {
                        record.LineItemNumber = tempLineItemNumber;
                    }
                    else
                    {
                        record.LineItemNumber = 0;
                    }
                    record.PrimaryIdentifierType = (row.IsNull(48)) ? null : (string)row.ItemArray[48];
                    records.Add(record);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            lvAcquisitions.ItemsSource = records;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Status");
            view.GroupDescriptions.Add(groupDescription);

            view.SortDescriptions.Add(new SortDescription("Status", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("AcquisitionID", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("Supplier", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("Description", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("SerialNumber", ListSortDirection.Ascending));
            view.Filter = UserFilter;
        }
        private void GetStores()
        {
            try
            {
                // Exit the program if the user has no permissions
                if (currentUser.Stores.Count == 0 || !currentUser.IsActive)
                {
                    MBWindow.Show($"There are no stores associated with username '{currentUser.Name},' or you do not have access to this application. {Environment.NewLine}{Environment.NewLine} If you feel you've received this message in error, please contact the CSC Help Desk at (757) 282-1500.", "Access Denied", MessageBoxButton.OK);
                    this.Close();
                }

                // Bind to the combobox
                cboxSelectedStore.ItemsSource = currentUser.Stores;
                cboxSelectedStore.DisplayMemberPath = "StoreDescription";
                cboxSelectedStore.SelectedValuePath = "StoreID";
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Store assignment failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void BtnAddAcquisition_Click(object sender, RoutedEventArgs e)
        {
            string storeCode = (cboxSelectedStore.SelectedValue == null) ? (cboxSelectedStore.Items[0] as Store).StoreID : cboxSelectedStore.SelectedValue.ToString();
            AddEditAcquisitionWindow addEditAcquisitionWindow = new AddEditAcquisitionWindow(storeCode, currentUser.Stores);
            addEditAcquisitionWindow.ShowDialog();

            if (addEditAcquisitionWindow.Result == WindowResult.OK)
            {
                GetAcquisitions();
            }
        }
        private void BtnAddDisposition_Click(object sender, RoutedEventArgs e)
        {
            Record selectedrecord = lvAcquisitions.SelectedItem as Record;
            AddEditDispositionWindow addEditDispositionWindow = new AddEditDispositionWindow(selectedrecord.AcquisitionID);
            addEditDispositionWindow.ShowDialog();
            int position = lvAcquisitions.SelectedIndex;

            if (addEditDispositionWindow.Result == WindowResult.OK)
            {
                GetAcquisitions();
                lvAcquisitions.SelectedIndex = position;
            }
        }
        private void BtnDeleteDisposition_Click(object sender, RoutedEventArgs e)
        {
            if (lvAcquisitions.SelectedIndex == -1)
                return;

            int selectionIndex = lvAcquisitions.SelectedIndex;

            if (MBWindow.Show($"Are you sure you want to delete Disposition {(lvAcquisitions.SelectedItem as Record).DispositionID}?", "Confirm Delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                (lvAcquisitions.SelectedItem as Record).DeleteDisposition();
                CollectionViewSource.GetDefaultView(lvAcquisitions.ItemsSource).Refresh();
                lvAcquisitions.SelectedIndex = selectionIndex;
            }
        }
        private void BtnEditDisposition_Click(object sender, RoutedEventArgs e)
        {
            Record selectedrecord = lvAcquisitions.SelectedItem as Record;
            AddEditDispositionWindow addEditDispositionWindow = new AddEditDispositionWindow(selectedrecord);
            addEditDispositionWindow.ShowDialog();
            int selectedRecordIndex = lvAcquisitions.SelectedIndex;

            if (addEditDispositionWindow.Result == WindowResult.OK)
            {
                GetAcquisitions();
                lvAcquisitions.SelectedIndex = selectedRecordIndex;
            }
        }
        private void BtnEditAcquisiton_Click(object sender, RoutedEventArgs e)
        {
            Record selectedrecord = lvAcquisitions.SelectedItem as Record;
            AddEditAcquisitionWindow addEditAcquisitionWindow = new AddEditAcquisitionWindow(selectedrecord, currentUser.Stores);
            addEditAcquisitionWindow.ShowDialog();
            int selectedRecordIndex = lvAcquisitions.SelectedIndex;

            if (addEditAcquisitionWindow.Result == WindowResult.OK)
            {
                GetAcquisitions();
                lvAcquisitions.SelectedIndex = selectedRecordIndex;
            }
        }
        private void txtGotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }
        private void txtGotMouseCapture(object sender, MouseEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }

        private void MenuManageUsers_Click(object sender, RoutedEventArgs e)
        {
            UserManagementWindow umw = new UserManagementWindow();
            umw.Show();
        }

        private void MenuOpenAcquisitions_Click(object sender, RoutedEventArgs e)
        {
            ReportConsole reportConsole = new ReportConsole(ReportType.OpenAcquisition, currentUser, (cboxSelectedStore.SelectedItem as Store));
            reportConsole.ShowDialog();
        }

        private void MenuDispositions_Click(object sender, RoutedEventArgs e)
        {
            ReportConsole reportConsole = new ReportConsole(ReportType.Disposition, currentUser, (cboxSelectedStore.SelectedItem as Store));
            reportConsole.ShowDialog();
        }

        private void MenuApplicationSettings_Click(object sender, RoutedEventArgs e)
        {
            ApplicationSettingsWindow applicationSettingsWindow = new ApplicationSettingsWindow();
            applicationSettingsWindow.ShowDialog();

            if (applicationSettingsWindow.Result == WindowResult.OK)
            {
                applicationSettings.LoadFromFile();
                if (applicationSettings.UseCustomAcquisitonBeginningDate)
                    dtpAcquisitionFromDateFilter.SelectedDate = applicationSettings.CustomAcquisitionBeginningDate;
            }            
        }

        private void MenuEditRecord_Click(object sender, RoutedEventArgs e)
        {
            EditRecordWindow editRecordWindow = new EditRecordWindow(currentUser);
            editRecordWindow.ShowDialog();
        }
    }

    public class SortAdorner : Adorner
    {
        private static readonly Geometry ascGeometry =
            Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

        private static readonly Geometry descGeometry =
            Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

        public ListSortDirection Direction { get; private set; }

        public SortAdorner(UIElement element, ListSortDirection dir)
            : base(element)
        {
            this.Direction = dir;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (AdornedElement.RenderSize.Width < 20)
                return;

            TranslateTransform transform = new TranslateTransform
                (
                    AdornedElement.RenderSize.Width - 15,
                    (AdornedElement.RenderSize.Height - 5) / 2
                );
            drawingContext.PushTransform(transform);

            Geometry geometry = ascGeometry;
            if (this.Direction == ListSortDirection.Descending)
                geometry = descGeometry;
            drawingContext.DrawGeometry(Brushes.Black, null, geometry);

            drawingContext.Pop();
        }
    }
    public class WidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int columnsCount = System.Convert.ToInt32(parameter);
            double width = (double)value;
            return width / columnsCount;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SplitWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (double)value * 0.5;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
