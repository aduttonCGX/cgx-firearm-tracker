﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;
using Firearm;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddEditUserWindow.xaml
    /// </summary>
    public partial class AddEditUserWindow : Window
    {
        private bool buttonDepressed = false;
        private bool editMode = false;
        public User User;
        public WindowResult Result;
        private List<string> usedUserNames;

        public AddEditUserWindow()
        {
            InitializeComponent();
            Title = "Add New User";
            lblTitle.Text = Title;
            lblUsernameValidationText.Visibility = Visibility.Hidden;
            User = new User();
            PopulateUserData();
            GetUsedUsers();
            txtWindowsUsername.Focus();
            lvStorePermissions.IsEnabled = !User.IsAdmin;
        }

        public AddEditUserWindow(User user)
        {
            InitializeComponent();
            editMode = true;
            Title = "Edit User";
            lblTitle.Text = Title;
            txtWindowsUsername.IsEnabled = false;
            lblUsernameValidationText.Visibility = Visibility.Hidden;
            this.User = user;
            PopulateUserData();
            lvStorePermissions.IsEnabled = !User.IsAdmin;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtWindowsUsername.Text))
            {
                MBWindow.Show("Must provide a username.", "Error", MessageBoxButton.OK);
                return;
            }
            if (lblUsernameValidationText.Visibility == Visibility.Visible)
            {
                MBWindow.Show("Please select a new username.", "Validation Error", MessageBoxButton.OK);
                return;
            }
            
            if (editMode)
            {
                UpdateDatabase();
            }
            else
            {
                AddToDatabase();
            }
        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }
        private void AddToDatabase()
        {
            // Create the user
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    
                    SqlCommand cmd = new SqlCommand("sp_AddApplicationUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Username", txtWindowsUsername.Text));
                    cmd.Parameters.Add(new SqlParameter("@IsAdmin", cbAdmin.IsChecked));
                    cmd.Parameters.Add(new SqlParameter("@IsActive", cbActive.IsChecked));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Get User ID
            string UserID;
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string query = $@"SELECT [UserID] FROM [CGESFirearm].[dbo].[users] WHERE [UserName] = '{txtWindowsUsername.Text}'";
                    SqlCommand sqlcommand = new SqlCommand(query, conn);
                    UserID = sqlcommand.ExecuteScalar().ToString();

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Create permissions
            List<UserStorePermission> storePermissions = (List<UserStorePermission>)lvStorePermissions.ItemsSource;
            foreach (UserStorePermission permission in storePermissions)
            {
                if (permission.HasPermission)
                {
                    try
                    {
                        using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                        {
                            conn.Open();

                            SqlCommand cmd = new SqlCommand("sp_AddUserStorePermission", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                            cmd.Parameters.Add(new SqlParameter("@StoreID", permission.StoreID));
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex);
                        MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            // User creation successful
            Result = WindowResult.OK;
            MBWindow.Show($"User added successfully.", "Notice", MessageBoxButton.OK);
            Close();
        }

        private void UpdateDatabase()
        {
            // Update the user
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_UpdateUser", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserName", txtWindowsUsername.Text));
                    cmd.Parameters.Add(new SqlParameter("@IsAdmin", cbAdmin.IsChecked));
                    cmd.Parameters.Add(new SqlParameter("@IsActive", cbActive.IsChecked));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Get User ID
            string UserID;
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    string query = $@"SELECT [UserID] FROM [CGESFirearm].[dbo].[users] WHERE [UserName] = '{txtWindowsUsername.Text}'";
                    SqlCommand sqlcommand = new SqlCommand(query, conn);
                    UserID = sqlcommand.ExecuteScalar().ToString();

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database read failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Clear permissions
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_RemoveUserStorePermissions", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }

            // Update permissions
            List<UserStorePermission> storePermissions = (List<UserStorePermission>)lvStorePermissions.ItemsSource;            
            foreach (UserStorePermission permission in storePermissions)
            {
                if (permission.HasPermission)
                {
                    try
                    {
                        using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                        {
                            conn.Open();

                            SqlCommand cmd = new SqlCommand("sp_AddUserStorePermission", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                            cmd.Parameters.Add(new SqlParameter("@StoreID", permission.StoreID));
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex);
                        MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                        return;
                    }
                }
            }

            // User creation successful
            Result = WindowResult.OK;
            MBWindow.Show($"User updated successfully.", "Notice", MessageBoxButton.OK);
            Close();
        }

        private void PopulateUserData()
        {
            txtWindowsUsername.Text = User.Name;
            cbActive.IsChecked = User.IsActive;
            cbAdmin.IsChecked = User.IsAdmin;
            lvStorePermissions.ItemsSource = User.Permissions;
        }

        private void TxtWindowsUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editMode)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(txtWindowsUsername.Text))
            {
                lblUsernameValidationText.Visibility = Visibility.Hidden;
                return;
            }

            if (usedUserNames.FirstOrDefault(element => element.Equals(txtWindowsUsername.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) != null)
            {
                lblUsernameValidationText.Visibility = Visibility.Visible;
            }
            else
            {
                lblUsernameValidationText.Visibility = Visibility.Hidden;
            }
        }
        private void GetUsedUsers()
        {
            usedUserNames = new List<string>();
            string query = @"SELECT UserName FROM [CGESFirearm].[dbo].[users]";

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection);

                cmd.CommandTimeout = 600;

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    usedUserNames.Add((string)row.ItemArray[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }

        private void CbAdmin_Checked(object sender, RoutedEventArgs e)
        {
            lvStorePermissions.IsEnabled = !(bool)cbAdmin.IsChecked;
            if ((bool)cbAdmin.IsChecked)
            {
                foreach (UserStorePermission permission in (List<UserStorePermission>)lvStorePermissions.ItemsSource)
                {
                    permission.HasPermission = true;
                }
                CollectionViewSource.GetDefaultView(lvStorePermissions.ItemsSource).Refresh();
            }
        }
    }
}
