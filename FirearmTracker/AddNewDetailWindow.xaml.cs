﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Utilities;

namespace FirearmTracker
{
    /// <summary>
    /// Interaction logic for AddNewDetailWindow.xaml
    /// </summary>
    /// 
    public enum DetailType { Manufacturer, Type, CaliberGauge }

    public partial class AddNewDetailWindow : Window
    {
        private bool buttonDepressed = false;
        public WindowResult Result;
        private List<string> existingData;
        private DetailType inputDetailType;
        public string Type;
        public string Manufacturer;
        public string CaliberOrGauge;

        public AddNewDetailWindow(DetailType detailType)
        {
            InitializeComponent();
            txtInput.Focus();
            inputDetailType = detailType;

            lblInputValidationText.Text = "";

            GetExistingData();

            switch (inputDetailType)
            {
                case (DetailType.Manufacturer):
                    lblInput.Text = "Manufacturer";
                    break;
                case (DetailType.Type):
                    lblInput.Text = "Type";
                    break;
                case (DetailType.CaliberGauge):
                    lblInput.Text = "Caliber or Gauge";
                    break;
            }
            lblTitle.Text = $"Add New {lblInput.Text}";
            Title = lblTitle.Text;
        }

        private void btn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    Button currentButton = sender as Button;
                    currentButton.ClearValue(EffectProperty);

                    Thickness m = currentButton.Margin;
                    m.Top = currentButton.Margin.Top + (float)3;
                    currentButton.Margin = m;
                    buttonDepressed = true;
                    break;
                default:
                    break;
            }
        }
        private void btn_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (buttonDepressed)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Button currentButton = sender as Button;
                        Brush brush = currentButton.Background;
                        if (brush is SolidColorBrush)
                        {
                            string colorValue = ((SolidColorBrush)brush).Color.ToString();
                        }

                        currentButton.Effect = new System.Windows.Media.Effects.DropShadowEffect
                        {
                            BlurRadius = 5,
                            Direction = 315,
                            Opacity = 1,
                            ShadowDepth = 3
                        };

                        Thickness m = currentButton.Margin;
                        m.Top = currentButton.Margin.Top - (float)3;
                        currentButton.Margin = m;
                        buttonDepressed = false;
                        break;
                    default:
                        break;
                }
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            AddItemToDatabase();
        }
        private void AddItemToDatabase()
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text))
            {
                MBWindow.Show("All fields are required.", "Error", MessageBoxButton.OK);
                return;
            }

            if (lblInputValidationText.Text != "")
            {
                MBWindow.Show("Please correct validation errors.", "Error", MessageBoxButton.OK);
                return;
            }

            // Add to database
            try
            {
                using (SqlConnection conn = new SqlConnection(SQLHelper.ConnectionString))
                {
                    conn.Open();
                    switch (inputDetailType)
                    {
                        case (DetailType.Manufacturer):
                            SqlCommand manufacturerCmd = new SqlCommand("sp_AddManufacturer", conn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            manufacturerCmd.Parameters.Add(new SqlParameter("@ManufacturerDescription", txtInput.Text));
                            manufacturerCmd.ExecuteNonQuery();
                            break;
                        case (DetailType.Type):
                            SqlCommand typeCmd = new SqlCommand("sp_AddFirearmType", conn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            typeCmd.Parameters.Add(new SqlParameter("@TypeDescription", txtInput.Text));
                            typeCmd.ExecuteNonQuery();
                            break;
                        case (DetailType.CaliberGauge):
                            SqlCommand caliberGaugeCmd = new SqlCommand("sp_AddCaliberGauge", conn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            caliberGaugeCmd.Parameters.Add(new SqlParameter("@CaliberGaugeDescription", txtInput.Text));
                            caliberGaugeCmd.ExecuteNonQuery();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database write failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
            finally
            {
                Result = WindowResult.OK;
                MBWindow.Show($"Item added successfully.", "Notice", MessageBoxButton.OK);
                Close();
            }
        }
        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            Result = WindowResult.Cancel;
            Close();
        }
        private void TxtInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtInput.Text))
            {
                lblInputValidationText.Text = "";
                return;
            }                

            if (existingData.FirstOrDefault(element => element.Equals(txtInput.Text.Trim(), StringComparison.CurrentCultureIgnoreCase)) != null)
            {
                lblInputValidationText.Text = "Item already exists.";
            }
            else
            {
                lblInputValidationText.Text = "";
            }
        }
        private void GetExistingData()
        {
            string query = "";
            existingData = new List<string>();

            switch (inputDetailType)
            {
                case (DetailType.Manufacturer):
                    query = @"SELECT ManufacturerDescription FROM [CGESFirearm].[dbo].[manufacturers]";
                    break;
                case (DetailType.Type):
                    query = @"SELECT TypeDescription FROM [CGESFirearm].[dbo].[types]";
                    break;
                case (DetailType.CaliberGauge):
                    query = @"SELECT CaliberGaugeDescription FROM [CGESFirearm].[dbo].[caliberGauge]";
                    break;
            }

            // Check database
            SqlConnection connection = new SqlConnection(SQLHelper.ConnectionString);
            try
            {
                connection.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter adapter = new SqlDataAdapter();
                SqlCommand cmd = new SqlCommand(query, connection)
                {
                    CommandTimeout = 600
                };

                // Acquire the data
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);

                // Store the data
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    existingData.Add((string)row.ItemArray[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MBWindow.Show($"Database lookup failed: {ex.Message}", "Error", MessageBoxButton.OK);
                return;
            }
        }
        private void TxtInput_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case (Key.Enter):
                    AddItemToDatabase();
                    break;
                case (Key.Escape):
                    Result = WindowResult.Cancel;
                    Close();
                    break;
                default:
                    break;
            }
        }
    }
}
